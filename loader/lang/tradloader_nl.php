<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=nl
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Met de installatie beginnen >>',
	'bouton_suivant_maj' => 'De aanpassing starten >>',

	// C
	'ce_repertoire' => 'van deze lijst',

	// D
	'donnees_incorrectes' => '<h4>Incorrecte gegevens. Probeer opnieuw of gebruik de handmatige installatie.</h4>
  <p>Geproduceerde fout:  @erreur@</p>',
	'du_repertoire' => 'van de lijst',

	// E
	'echec_chargement' => '<h4>Het downloaden is niet geslaagd. Gelieve opnieuw proberen, of de handmatige installatie gebruiken.</h4>',
	'echec_php' => 'Jouw versie van PHP @php1@ is niet compatibel met deze versie van SPIP die ten minste PHP @php2@ vereist.',

	// S
	'spip_loader_maj' => 'Versie @version@ van spip_loader.php is beschikbaar.',

	// T
	'texte_intro' => '<p>Het programma gaat de bestanden van @paquet@ downloaden in @dest@.</p>',
	'texte_preliminaire' => '<br /><h2>Voorafgaand: <b>De toegangsrechten regelen</b></h2>
  <p><b>In de huidige map kan niet worden geschreven.</b></p>
  <p>Om te verhelpen, gebruikt je een FTP client waarmee je de toegangsrechten van deze map instelt (installatiemap van @paquet@).<br />
  De procedure wordt in detail uitgelegd in de installatiegids. Je kunt kiezen uit:</p>
  <ul>
  <li><b>Als je een grafische FTP client hebt</b>, stel dan de rechten van de huidige map in zodat iedereen erin kan schrijven.</li>
  <li><b>Heb je een FTP client in tekst modus</b>, stel de rechten van de map dan in met @chmod@.</li>
  <li><b>Als je toegang hebt via Telnet</b>, doe dan een <i>chmod @chmod@ repertoire_courant</i>.</li>
  </ul>
  <p>Wanneer je dit gedaan hebt, zul je <b><a href=\'@href@\'>deze bladzijde kunnen </a></b> opladen om met de download en de installatie te beginnen.</p>
  <p>Als de fout voortduurt, zul je de klassieke installatieprocedure moeten gebruiken
  (download van alle bestanden met FTP).</p>',
	'titre' => 'Download van @paquet@',
	'titre_maj' => 'Aanpassing van @paquet@',
	'titre_version_courante' => 'Momenteel geïnstalleerde versie: ',
	'titre_version_future' => 'Installatie van versie: '
);
