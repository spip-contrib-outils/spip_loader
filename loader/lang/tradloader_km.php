<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=km
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'ផ្តើម ការតំលើង >>',

	// C
	'ce_repertoire' => 'ថតឯកសារនេះ',

	// D
	'du_repertoire' => 'ថតឯកសារ',

	// T
	'texte_preliminaire' => '<br /><h2>Preliminary step: <b>Set the access permissions</b></h2>
 <p><b>It is not possible to write to the
current directory.</b></p>
 <p>To change the permissions of the directory in
which you are installing @paquet@ use your FTP client.<br /> The procedure is explained in detail in the Installation Guide. Choose between:</p>
 <ul>
 <li><b>If you have an FTP client with a graphical interface</b>, set the permissions
of the directory to make it open for everyone to write to it.</li>
 <li><b>If you have an FTP client with a text interface</b>, change the permissions of the directory to the value @chmod@.</li>
 <li><b>If you are using a Telnet access</b>,
execute the command <i>chmod @chmod@ current_directory</i>.</li>
 </ul>
<p>Once this has been done, please <b><a href=\'@href@\'>reload this page</a></b>
 to start to download and install SPIP.</p>
 <p>If you continue to receive this error notification, you will need to use the manualinstallation method
 (downloading the SPIP files by FTP) instead.</p>',
	'titre' => 'ទំនាញយក នៃ @paquet@'
);
