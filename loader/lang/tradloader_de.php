<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=de
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Installation starten >>',
	'bouton_suivant_maj' => 'Update starten >>',

	// C
	'ce_repertoire' => 'dieses Ordners',

	// D
	'donnees_incorrectes' => '<h4>Falsche Daten. Bitte versuchen Sie es erneut, oder installieren Sie SPIP manuell.</h4>
  <p>Fehler: @erreur@</p>',
	'du_repertoire' => 'des Ordners',

	// E
	'echec_chargement' => '<h4>Laden nicht möglich. Bitte versuchen Sie es erneut, oder installieren Sie SPIP manuell.</h4>',
	'echec_php' => 'Ihre PHP-Version @php1@ ist nicht mit dieser Version von SPIP kompatibel. Sie benötigen mindestens PHP @php2@.',

	// S
	'spip_loader_maj' => 'Die Version @version@ von spip_loader.php ist verfügbar.',

	// T
	'texte_intro' => '<p>Das Programm wird die Dateien von @paquet@ in @dest@ herunterladen.</p>',
	'texte_preliminaire' => '<br /><h2>Vorbereitung: <b>Zugriffsrechte einstellen</b></h2>
<p><b>In das aktuelle Verzeichnis kann nicht geschrieben werden.</b>
<p>Bitte verwenden Sie Ihr FTP-Programm, um die Zugriffsrechte für das
 Installationsverzeichnis von @paquet@ einzustellen. Die Installationsanleitung erläutert die Vorgehensweise:/p>
<ul>
<li><b>Mit einem grafischen FTP-Client</b>, stellen Sie die Zugriffsrechte so ein, dass jeder in das Verzeichnis schreiben darf.</li>
<li><b>Wenn Sie einen textbasierten FTP-Client verwenden</b>, ändern Sie den Modus des Verzeichnis nach @chmod@.</b><></li>
<li><b>Wenn Sie einen Telnetzugang haben</b>, führen Sie <i>chmod @chmod@ aktuelles_verzeichnis</i> aus.</li>
</ul>
<p>Wenn Sie diese Änderung durchgeführt haben, können Sie <b><a href=\'@href@\'>diese Seite neu laden</a></b>, um SPIP herunterzuladen und die Installation zu beginnen.</p>
<p>Falls der Fehler weiter auftritt, können Sie die klassische Installation durchführen (Kopieren Sie alle Dateien per FTP auf Ihren Server).</p>',
	'titre' => 'Herunterladen von @paquet@',
	'titre_maj' => 'Update von @paquet@',
	'titre_version_courante' => 'Zur Zeit installierte Version : ',
	'titre_version_future' => 'Installation von Version : '
);
