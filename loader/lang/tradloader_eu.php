<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=eu
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Instalazioa abiatu',

	// C
	'ce_repertoire' => 'direktorio hunetako',

	// D
	'donnees_incorrectes' => '<h4> Baliogabeko datuak. Betse entsegu bat egin, edo eskuzko instalazioa baliatu.</h4>
  <p>Gertatutako errorea: @erreur@</p>',
	'du_repertoire' => 'direktoriokoa',

	// E
	'echec_chargement' => '<h4>Kargatzeak hutsegin du. beste entsegu bat egin, edo eskuzko instalazioa erabili.</h4>',

	// T
	'titre' => '@paquet@-ren telekargatzea'
);
