<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=zh_tw
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => '開始安裝 >>',

	// C
	'ce_repertoire' => '該目錄',

	// D
	'donnees_incorrectes' => '<h4>資料錯誤。請重新開始，或是自行手動安裝。</h4>
  <p>產生的錯誤 : @erreur@</p>',
	'du_repertoire' => '目錄',

	// T
	'titre' => '下載套件 @paquet@'
);
