<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=eo
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Komenci instalprocezon >>',
	'bouton_suivant_maj' => 'Komenci la ĝisdatigon >>',

	// C
	'ce_repertoire' => 'tiu dosierujo',

	// D
	'donnees_incorrectes' => '<h4>Datenoj ne korektaj. Bonvolu reprovi, aŭ faru instalprocezon mane. </h4>
  <p>Eraro okazinta : @erreur@</p>',
	'du_repertoire' => 'la dosierujo',

	// E
	'echec_chargement' => '<h4>Ŝargo malsukcesis. Bonvolu reprovi, aŭ faru instalprocezon mane.</h4>',
	'echec_php' => 'Via PHP-versio @php1@ ne kongruas kun tiu versio de SPIP, kiu bezonas almenaŭ PHP @php2@.',

	// S
	'spip_loader_maj' => 'Version @version@ de spip_loader.php disponeblas.',

	// T
	'texte_intro' => '<p>La programo tuj lanĉos la elŝuton de la dosieroj de @paquet@ enen de @dest@.</p>',
	'texte_preliminaire' => '<br /><h2>Antaŭfaroj : <b>Agordu la alirratojn</b></h2>
  <p><b>La kuranta dosierujo ne estas skribe modifebla.</b></p>
  <p>Por tion ŝanĝi, uzu vian FTP-klienton por agordi alirrajtojn
  al tiu dosierujo (dosierujo por instali @paquet@).<br />
  La proceduro estas detale priskribita en la instalgviddokumento. Laŭ elekto :</p>
  <ul>
  <li><b>Se vi havas grafikan FTP-klienton</b>, agordu la trajtojn de la kuranta dosierujo
  por ke ĝi estu skribe modifebla far ĉiuj.</li>
  <li><b>Se via FTP-kliento funkcias laŭ teksta modo</b>, ŝanĝu la alirstatuson de la dosierujo al la valoro @chmod@.</li>
  <li><b>Se vi havas Telnetan aliron</b>, tajpu <i>chmod @chmod@ kuranta_dosierujo</i>.<li>
  </ul>
  <p>Kiam tiu faro estas plenumita, vi povos <b><a href=\'@href@\'>reŝargi tiun paĝon</a></b>
  por komenci la elŝuton kaj la instalprocezon.</p>
  <p>Se la eraro daŭras, vi ekprovu per klasika instalproceduro
  (elŝutado de ĉiuj dosieroj per FTP).</p>',
	'titre' => 'Elŝuto de @paquet@',
	'titre_maj' => 'Ĝisdatigo de @paquet@',
	'titre_version_courante' => 'Nun instalita versio :',
	'titre_version_future' => 'Instalota versio :'
);
