<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=ru
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Установить >>',

	// C
	'ce_repertoire' => 'эта папка',

	// D
	'donnees_incorrectes' => '<h4>Неправильная информация. Попробуйте еще раз или попробуйте установить SPIP вручную.</h4>
<p>Ошибка : @erreur@</p>',
	'du_repertoire' => 'папка',

	// E
	'echec_chargement' => '<h4>Не удалось скачать установочный файл. Попробуйте позже или установите SPIP вручную.</h4>',

	// T
	'texte_intro' => '<p><b>Добро пожаловать в систему автоматической установки @paquet@.</b> <p>Сначала программа устанвоки проверит права доступа на текущую папку и попробует автоматически скачать установочный файл @paquet@ в  @dest@. <p>Нажмите на кнопку для начала установки.', # MODIF
	'titre' => 'Скачать @paquet@'
);
