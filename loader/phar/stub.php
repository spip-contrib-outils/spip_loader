<?php

namespace Spip\Loader;

use Phar;

final class Stub {
	const VERSION = '';
	const FULL_VERSION = '';
	const DATE = '';
	const DEBUG = false;

	const NAME = 'spip_loader.phar';
	const AUTOLOADER = 'phar://spip_loader.phar/vendor/autoload.php';

	/** @return void */
	public function init() {
		Phar::mapPhar(self::NAME);
		if ($this->is_opcache_enabled()) {
			// some hosting failed re-opening inside phar files :/
			// and some weird cache occurs sometimes into phar content files
			@ini_set('opcache.enable', 0);
		}
		require self::AUTOLOADER;
		Debug::add('FULL_VERSION', self::VERSION);
		Debug::add('DATE', self::DATE);
	}

	/** @return void */
	public function handle() {
		if ($this->is_route('warmup') && function_exists('opcache_reset')) {
			// Route `spip_loader.php?warmup` that could help on some hosting and some opcache configs.
			// Please don’t use this abusively as it clears cache for all files in the fpm instance (not only this website)
			@opcache_reset();
			Debug::add('Reset Opcache', 'Script reset Opcache.');
		}

		if (!http_response_code()) {
			$this->handle_cli();
		} else {
			$this->handle_web();
		}
	}

	/** @return never */
	public function handle_cli() {
		require 'phar://spip_loader.phar/bin/console.php';
		exit(0);
	}

	/** @return void */
	public function handle_web() {
		/** @phpstan-ignore-next-line */
		if (self::DEBUG) {
			Debug::enable();
		}

		Phar::webPhar('spip_loader.phar', null, null, ['svg' => 'image/svg+xml'], function ($incomming_path) {
			$path = $incomming_path;

			// Gestion des assets (spip_loader.php/?file=assets/...)
			if (isset($_GET['file'])) {
				$path = '/' . ltrim($_GET['file'], '/');
			}
			if (!$path) {
				$path = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
			}
			if (!$path) {
				$path = (string) substr(
					parse_url(isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '', PHP_URL_PATH),
					strlen($_SERVER['PHP_SELF'])
				);
			}

			Debug::add('incomming path', $incomming_path);
			Debug::add('calculated path', $path);

			$extension = pathinfo($path, \PATHINFO_EXTENSION);

			if (in_array($extension, ['css', 'js', 'jpg', 'png', 'ico', 'svg'])) {
				# Éviter Failed to load resource: net::ERR_HTTP2_PROTOCOL_ERROR (http/2)
				# https://serverfault.com/questions/844526/apache-2-4-7-ignores-response-header-content-encoding-identity-instead-respect
				header('Content-Encoding: none');
				header('Expires: ' . \date('r', time() + 86400 * 7));
			}
			if ($extension && $extension !== 'php') {
				return '/public' . $path;
			}
			if ($path === '') {
				return '/index.php';
			}
			return false;
		});
	}

	/**
	 * @param \Throwable $e
	 * @return void
	 */
	public static function show_on_debug($e) {
		/** @phpstan-ignore-next-line */
		if (!self::DEBUG) {
			throw $e;
		}
		/** @phpstan-ignore-next-line */
		echo $e->getMessage() . ' on file ' . $e->getFile() . ' on line ' . $e->getLine();
	}

	/** @return bool */
	private function is_opcache_enabled() {
		return function_exists('opcache_get_status')
			&& @opcache_get_status()
			&& (@opcache_get_status()['opcache_enabled']);
	}

	/**
	 * Simple route `spip_loader.php?{route}`
	 * @param string $route
	 * @return bool
	 */
	private function is_route($route) {
		return !empty($_SERVER['QUERY_STRING']) && ($_SERVER['QUERY_STRING'] === $route);
	}
}

try {
	$stub = new Stub();
	$stub->init();
	$stub->handle();
} catch (\Exception $e) {
	// @note use \Throwable when PHP 7+
	Stub::show_on_debug($e);
	/** @phpstan-ignore-next-line */
} catch (\Error $e) {
	Stub::show_on_debug($e);
}

__HALT_COMPILER();
