<?php

namespace Spip\Loader;

use Spip\Loader\Config\Config;

class Deprecations {
	/** @var Config */
	private $config;

	/** @var array<mixed> */
	private $deprecations = [];

	public function __construct(Config $config) {
		$this->config = $config;
	}

	/**
	 * @return void
	 */
	public function handle() {
		$this->deprecations = [];

		if (defined('_SPIP_LOADER_UPDATE_AUTEURS')) {
			/** Auteur(s) autorise(s) a proceder aux mises a jour : '1:2:3' */
			$this->config->set('authorized.users', explode(':', _SPIP_LOADER_UPDATE_AUTEURS));
			$this->deprecate_config_array('_SPIP_LOADER_UPDATE_AUTEURS', 'authorized.users');
		}

		// Url des versions proposées
		if (defined('_URL_SPIP_LOADER_LIST')) {
			$this->config->set('url.spip_loader.api', _URL_SPIP_LOADER_LIST);
			$this->deprecate_config_string('_URL_SPIP_LOADER_LIST', 'url.spip_loader.api');
		}

		# telecharger a travers un proxy
		if (defined('_URL_LOADER_PROXY')) {
			$this->config->set('url.spip_loader.proxy', constant('_URL_LOADER_PROXY'));
			$this->deprecate_config_string('_URL_LOADER_PROXY', 'url.spip_loader.proxy');
		}

		if (defined('_URL_LOADER_DL')) {
			// Url du site du fichier spip_loader permettant de tester sa version distante
			$this->config->set('url.spip_loader.php', _URL_LOADER_DL . 'spip_loader.php');
			$this->deprecate_config_string('_URL_LOADER_DL', 'url.spip_loader.php');
		}

		// Url du fichier spip_loader permettant de tester sa version distante
		if (defined('_URL_SPIP_LOADER')) {
			$this->config->set('url.spip_loader.php', _URL_SPIP_LOADER);
			$this->deprecate_config_string('_URL_SPIP_LOADER', 'url.spip_loader.php');
		}

		# repertoires d'installation
		if (defined('_DIR_BASE')) {
			$this->config->set('directory.install', _DIR_BASE);
			$this->deprecate_config_string('_DIR_BASE', 'directory.install');
		}

		if (defined('_CHEMIN_FICHIER_ZIP')) {
			$this->config->set('archive.zip.path', constant('_CHEMIN_FICHIER_ZIP'));
			$this->deprecate_config_string('_CHEMIN_FICHIER_ZIP', 'archive.zip.path');
		}
		if (defined('_NOM_PAQUET_ZIP')) {
			$this->config->set('archive.zip.name', constant('_NOM_PAQUET_ZIP'));
			$this->deprecate_config_string('_NOM_PAQUET_ZIP', 'archive.zip.name');
		}

		# adresse du depot
		if (defined('_URL_SPIP_DEPOT')) {
			$this->config->set('url.spip.depot', _URL_SPIP_DEPOT);
			$this->deprecate_config_string('_URL_SPIP_DEPOT', 'url.spip.depot');
		}

		if (defined('_SPIP_LOADER_KEEP')) {
			$this->config->set('cleaner.keep.directory', _SPIP_LOADER_KEEP);
			$this->deprecate_config_string('_SPIP_LOADER_KEEP', 'cleaner.keep.directory');
		}

		if (defined('_DIR_PLUGINS')) {
			$this->removed_config('_DIR_PLUGINS');
		}

		if (defined('_SPIP_LOADER_PLUGIN_RETOUR')) {
			$this->removed_config('_SPIP_LOADER_PLUGIN_RETOUR');
		}

		if (defined('_DEST_PAQUET_ZIP')) {
			// Ne servait pas ? en dehors d’un affichage de titre ?
			$this->removed_config('_DEST_PAQUET_ZIP');
		}

		if (defined('_PCL_ZIP_RANGE')) {
			$this->removed_config('_PCL_ZIP_RANGE');
		}

		if (defined('_REMOVE_PATH_ZIP')) {
			$this->removed_config('_REMOVE_PATH_ZIP');
		}

		if (defined('_PCL_ZIP_SIZE')) {
			// Ne servait pas
			$this->removed_config('_PCL_ZIP_SIZE');
		}

		// "habillage" optionnel
		// liste separee par virgules de fichiers inclus dans spip_loader
		// charges a la racine comme spip_loader.php
		// selon l'extension: include .php , .css et .js dans le <head> genere par spip_loader
		if (defined('_SPIP_LOADER_EXTRA')) {
			// Ne servait plus
			$this->removed_config('_SPIP_LOADER_EXTRA');
		}

		$this->deprecate_show_new_usage();
	}

	/**
	 * @param string $old
	 * @param string $new
	 * @return void
	 */
	private function deprecate_config_string($old, $new) {
		$this->deprecate_config($old, $new, "'" . $this->config->get($new) . "'");
	}

	/**
	 * @param string $old
	 * @param string $new
	 * @return void
	 */
	private function deprecate_config_array($old, $new) {
		$this->deprecate_config($old, $new, '[' . implode(', ', $this->config->get($new)) . ']');
	}

	/**
	 * @param string $old Nom de constante probablement
	 * @param string $new Clé de configuration
	 * @param string $exportValue Nouvelle valeur de configuration formattée
	 * @return void
	 */
	private function deprecate_config($old, $new, $exportValue) {
		trigger_error(
			sprintf(
				'Usage of \'%s\' is deprecated, use \'%s\' config instead',
				$old,
				$new
			),
			\E_USER_DEPRECATED
		);
		$this->deprecations[$new] = $exportValue;
	}

	/**
	 * @param string $old Nom de constante probablement
	 * @return void
	 */
	private function removed_config($old) {
		trigger_error(
			sprintf(
				'Usage of \'%s\' is removed. No replacement for now.',
				$old
			),
			\E_USER_DEPRECATED
		);
	}

	/** @return void */
	private function deprecate_show_new_usage() {
		if (!$this->deprecations) {
			return;
		}
		trigger_error(
			sprintf('Some config should be moved in proper key in your spip_loader_config.php file. Suggestion: %s', $this->formatConfigFile()),
			\E_USER_NOTICE
		);
	}

	/** @return string */
	private function formatConfigFile() {
		$config = "<?php return [\n";
		foreach ($this->deprecations as $key => $value) {
			$config .= "\t'$key' => $value,\n";
		}
		$config .= '];';
		return $config;
	}
}
