<?php

namespace Spip\Loader\Route;

use Spip\Loader\Api\Versions;
use Spip\Loader\Config\Config;
use Spip\Loader\Http\RequestInterface;
use Spip\Loader\Http\Response;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\I18n\Translator;
use Spip\Loader\Remote;
use Spip\Loader\Route\RouteInterface;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;
use Spip\Loader\Template\Utils;

class ArchiveSelection extends AbstractRoute implements RouteInterface {
	public function match() {
		return empty($this->request->get('etape'));
	}

	/**
	 * @return ResponseInterface
	 */
	public function handle(
		Config $config,
		Versions $versions,
		Spip $spip,
		Page $page,
		array $contexte = []
	) {
		$response = new Response();
		$version_installee = $spip->version_installee();
		$versions_spip = $versions->lister_versions_spip();

		$paquet = $contexte['paquet'];
		$range = $this->request->get('range');
		$version_spip_loader = $contexte['version'];

		$fichier = $config->get('directory.install') . basename($paquet);

		$translator = $page->getTranslator();

		$hidden = [
			'chemin' => $paquet,
			'range' => $range,
			'etape' => file_exists($fichier) ? 'decompresser' : 'charger'
		];

		// Version proposée à l'installation par défaut
		$versions_spip = $versions_spip['versions'];
		$version_future = '';
		if (isset($versions_spip[$paquet])) {
			$version_future = $versions_spip[$paquet]['version'];
			if ($versions_spip[$paquet]['etat'] == 'dev') {
				$version_future .= '-dev';
			}
		}
		$version_future_affichee = 'SPIP ' . $version_future;

		// notre branche est elle maintenue ? si non, on demande à sélectionner une autre branche
		// ou on n’a demandé déjà un spip spécifique...
		// ou on n’a pas de spip installé !
		if (
			!empty($_REQUEST['chemin'])
			or false !== strpos($version_installee, '-dev')
			or !$version_installee
		) {
			$presente = true;
		} else {
			$ma_branche = $versions->getBranchNameFromTag($version_installee);
			$branches = $versions->lister_branches_proposees();
			$presente = $ma_branche && isset($branches[$ma_branche]);
			if (!$presente) {
				$version_future_affichee = 'Sélectionnez…';
			}
		}

		if ($version_installee) {
			// Mise à jour
			$bloc_courant =
				'<div class="msg-alert info version-courante">'
				. $translator->translate('tradloader:titre_version_courante')
				. '<strong>' . 'SPIP ' . $version_installee . '</strong>'
				. '</div>';
			$bouton = $translator->translate('tradloader:bouton_suivant_maj');
		} else {
			// Installation nue
			$bloc_courant = '';
			$bouton = $translator->translate('tradloader:bouton_suivant');
		}

		// Détection d'une incompatibilité avec la version de PHP installée
		$php_incompatible = false;
		$version_php_spip = false;
		$version_php_installee = phpversion();
		if (isset($versions_spip[$paquet]['version'])) {
			$branche_future = $versions->branche_spip($versions_spip[$paquet]['version']);
			$version_php_spip = $versions->lister_branches_proposees($branche_future);
			$version_php_spip = $version_php_spip['php_min'];
			$php_incompatible = version_compare($version_php_spip, $version_php_installee, '>');
		}

		$class = ($version_future === 'dev' or strpos($version_future, '-dev') !== false) ? 'notice' : 'success';
		if ($php_incompatible) {
			$class = 'error';
			$bouton =
				'<div class="msg-alert error">'
				. $translator->translate('tradloader:echec_php', ['php1' => $version_php_installee, 'php2' => $version_php_spip])
				. '</div>';
		} elseif (version_compare($version_installee, $version_future, '>') and ($version_future !== 'dev')) {
			// Épargnons un downgrade aux personnes étourdies
			$bouton =
				"<div class='boutons'>"
				. '<input type="submit" disabled="disabled" value="' . $bouton . '" />'
				. '</div>';
		} elseif (!$presente) {
			// Forcer à avoir une branche si la notre n’existe plus
			$bouton =
				"<div class='boutons'>"
				. '<input type="submit" disabled="disabled" value="' . $bouton . '" />'
				. '</div>';
		} else {
			$bouton =
				"<div class='boutons'>"
				. '<input type="submit" value="' . $bouton . '" />'
				. '</div>';
		}

		// Construction du corps
		if ($versions_spip) {
			$installDirectory = $config->get('directory.install');
			if ($installDirectory) {
				$nom = $translator->translate('tradloader:du_repertoire') . ' <code>' . $installDirectory . '</code>';
			} else {
				$nom = $translator->translate('tradloader:ce_repertoire');
			}

			$corps =
				$translator->translate('tradloader:texte_intro', [
					'paquet' => strtoupper($config->get('archive.zip.name')),
					'dest' => $nom
				])
				. '<div class="version">'
				. $bloc_courant
				. '<div class="msg-alert ' . $class . ' version-future">'
				. $translator->translate('tradloader:titre_version_future')
				. '<strong>' . $version_future_affichee . '</strong>'
				. Utils::menu_branches($config, $versions, $paquet, $version_installee, $presente)
				. '</div>'
				. '</div>'
				. $bouton;
		} else {
			$corps =
				'<div class="version">'
				. $bloc_courant
				. '<div class="msg-alert notice notice-version-future">'
				. 'Aucune version proposée ?<br><i>Probablement qu’une mise à jour de SPIP Loader s’impose !</i>'
				. '</div>'
				. '</div>';
		}

		$corps .= $this->blocMajLoader($config, $translator, $version_spip_loader);

		$title = $translator->translate(
			$version_installee ? 'tradloader:titre_maj' : 'tradloader:titre',
			['paquet' => strtoupper($config->get('archive.zip.name'))]
		);
		$page->setTitle($title);
		$page->setHiddens($hidden);
		$page->withLangMenu(); # ! après hiddens
		$page->setInner($corps);
		return $response->setContent($page->toHTML());
	}

	/**
	 * @param string $version_spip_loader
	 * @return string
	 */
	public function blocMajLoader(Config $config, Translator $translator, $version_spip_loader) {
		if ($version_spip_loader === 'dev') {
			return '';
		}

		$remote = new Remote($config);
		$remote_version = $remote->recuperer_version_spip_loader();
		if (
			$this->necessite_maj_spip_loader(
				$version_spip_loader,
				$remote_version
			)
		) {
			return
				"<div class='msg-alert notice'><a href='" . $config->get('url.spip_loader.php') . "'>"
				. $translator->translate('tradloader:spip_loader_maj', ['version' => $remote_version])
				. '</a>'
				. "<div class='boutons boutons-self-update'>"
				. "<button type='submit' name='etape' value='" . SelfUpdate::ETAPE . "'>"
				. $translator->translate('tradloader:bouton_suivant_maj')
				. '</button>'
				. '</div></div>';
		}
		return '';
	}

	/**
	 * @param string $local_version
	 * @param string $remote_version
	 * @return int|bool
	 */
	public static function necessite_maj_spip_loader($local_version, $remote_version) {
		return version_compare($local_version, $remote_version, '<');
	}

	/**
	 * @param Config $config
	 * @param Versions $versions
	 * @param Spip $spip
	 * @return string
	 */
	public static function selectArchive(
		Config $config,
		Versions $versions,
		Spip $spip,
		RequestInterface $request
	) {

		$paquet = '';
		if ($request->get('chemin')) {
			// Paquet indiqué dans l’URL
			$paquet = urldecode($request->get('chemin'));
		} elseif ($config->get('archive.zip.path')) {
			// Un paquet est expressement demandé en config
			$paquet = $config->get('archive.zip.path');
		} else {
			/** @var string[]|false Branche de destination par défaut */
			$notre_branche = $versions->determiner_branche_par_defaut();
			if ($notre_branche) {
				$paquet = $notre_branche['zip'];
			}
			$version_installee = $spip->version_installee();
			if ($version_installee) {
				if ($branche = $versions->lister_branches_proposees($versions->branche_spip($version_installee))) {
					$paquet = $branche['zip'];
				} elseif ((strpos($version_installee, '-dev') !== false) and $branche = $versions->lister_branches_proposees('dev')) {
					$paquet = $branche['zip'];
				}
			}
		}

		if (
			$paquet and (
				(strpos($paquet, '//') !== false) or
				(strpos($paquet, '../') !== false) or
				(substr($paquet, -4, 4) !== '.zip')
			)
		) {
			throw new \Exception("Chemin incorrect du paquet: $paquet");
		}

		return $paquet;
	}
}
