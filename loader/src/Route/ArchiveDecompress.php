<?php

namespace Spip\Loader\Route;

use Spip\Loader\Api\Versions;
use Spip\Loader\Config\Config;
use Spip\Loader\Http\Response;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\Route\RouteInterface;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

class ArchiveDecompress extends AbstractRoute implements RouteInterface {
	const ETAPE = 'decompresser';

	public function match() {
		# and file_exists($fichier)
		return $this->request->get('etape') === self::ETAPE;
	}

	/**
	 * @return ResponseInterface
	 */
	public function handle(
		Config $config,
		Versions $versions,
		Spip $spip,
		Page $page,
		array $contexte = []
	) {
		$response = new Response();
		$paquet = $contexte['paquet'];

		$archive = $config->get('directory.tmp') . basename($paquet);

		if (!(file_exists($archive))) {
			$page->setInner(
				'<div class="error">'
				. $page->getTranslator()->translate('tradloader:echec_chargement')
				. "<br><em>$paquet, $archive</em>"
				. '</div>'
			);
			return $response->setContent($page->toHTML())->setStatusCode(400);
		}

		$script = $config->get('app.filename');

		$dir_install = $config->get('directory.install') ?: './';

		if (class_exists('ZipArchive')) {
			$zip = new \ZipArchive();
			if (true === $zip->open($archive)) {
				$zip->extractTo($dir_install);
				$zip->close();
			} else {
				throw new \RuntimeException(sprintf('Can’t open archive "%s" with ZipArchive', $archive));
			}
		} elseif (PHP_VERSION_ID >= 70415) {
			$zip = new \PharData($archive);
			$zip->extractTo($dir_install, null, true);
		} else {
			throw new \RuntimeException('Can’t extract archive. Use PHP >= 7.4.15 or install php-zip extension');
		}

		// Afficher 90% et relancer pour les déplacements et nettoyages
		$url = sprintf('%s?etape=%s&chemin=%s', $script, ArchiveClean::ETAPE, $paquet);
		$title = $page->getTranslator()->translate('etape_' . ArchiveClean::ETAPE);
		return $response->setContent($page->redirige_boucle($url, $title));
	}
}
