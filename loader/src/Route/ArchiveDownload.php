<?php

namespace Spip\Loader\Route;

use Spip\Loader\Api\Versions;
use Spip\Loader\Config\Config;
use Spip\Loader\Http\RequestInterface;
use Spip\Loader\Http\Response;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\Remote;
use Spip\Loader\Route\RouteInterface;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

class ArchiveDownload extends AbstractRoute implements RouteInterface {
	const ETAPE = 'charger';

	public function match() {
		return $this->request->get('etape') === self::ETAPE;
	}

	/**
	 * @return ResponseInterface
	 */
	public function handle(
		Config $config,
		Versions $versions,
		Spip $spip,
		Page $page,
		array $contexte = []
	) {
		$response = new Response();
		$paquet = $contexte['paquet'];

		$script = $config->get('app.filename');

		$start = $this->request->get('start') ?: 0;

		if (!$start) {
			$url = sprintf('%s?etape=%s&chemin=%s&start=1', $script, self::ETAPE, $paquet);
			$title = $page->getTranslator()->translate('etape_' . self::ETAPE);
			return $response->setContent($page->redirige_boucle($url, $title));
		}

		$fichier = $config->get('directory.tmp') . basename($paquet);

		try {
			$remote = new Remote($config);
			$remote->copyTo($config->get('url.spip.depot') . $paquet, $fichier);
		} catch (\Exception $e) {
			$page->setInner(
				'<div class="error">'
				. $page->getTranslator()->translate('tradloader:echec_chargement')
				. "<br><em>{$e->getMessage()}</em>"
				. '</div>'
			);
			return $response->setContent($page->toHTML())->setStatusCode(400);
		}

		// Passer a l'etape suivante (desarchivage)
		$url = sprintf('%s?etape=%s&chemin=%s', $script, ArchiveDecompress::ETAPE, $paquet);
		$title = $page->getTranslator()->translate('etape_' . ArchiveDecompress::ETAPE);
		return $response->setContent($page->redirige_boucle($url, $title));
	}
}
