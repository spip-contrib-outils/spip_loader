<?php

namespace Spip\Loader\Route;

use Spip\Loader\Api\Versions;
use Spip\Loader\Config\Config;
use Spip\Loader\Filesystem;
use Spip\Loader\Http\Response;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\Remote;
use Spip\Loader\Route\RouteInterface;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

class SelfUpdate extends AbstractRoute implements RouteInterface {
	const ETAPE = 'selfupdate';

	public function match() {
		return $this->request->get('etape') === self::ETAPE;
	}

	/**
	 * {@inheritDoc}
	 */
	public function handle(Config $config, Versions $versions, Spip $spip, Page $page, array $contexte = []) {
		$response = new Response();
		$remote = new Remote($config);
		$url_spip_loader = $config->get('url.spip_loader.php');
		$name = basename($url_spip_loader);
		$remote->copyTo($url_spip_loader, $name . '.new');
		if (file_exists($name . '.new')) {
			rename($name . '.new', $name);
		}
		Filesystem::clear_opcode_cache(realpath(basename($url_spip_loader)));
		return $response->setContent($page->redirige_boucle($config->get('url.root') . basename($url_spip_loader)));
	}
}
