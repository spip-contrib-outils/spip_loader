<?php

namespace Spip\Loader\Route;

use Spip\Loader\Api\Versions;
use Spip\Loader\Config\Config;
use Spip\Loader\Http\RequestInterface;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

interface RouteInterface {
	/**
	 * @return bool
	 * */
	public function match();

	/**
	 * @param Config $config
	 * @param Versions $versions
	 * @param Spip $spip
	 * @param Page $page
	 * @param array<mixed> $contexte
	 * @return ResponseInterface
	 */
	public function handle(Config $config, Versions $versions, Spip $spip, Page $page, array $contexte);
}
