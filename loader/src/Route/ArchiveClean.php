<?php

namespace Spip\Loader\Route;

use Spip\Loader\Api\Versions;
use Spip\Loader\Cleaner;
use Spip\Loader\Config\Config;
use Spip\Loader\Http\Response;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\Route\RouteInterface;
use Spip\Loader\Session;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

class ArchiveClean extends AbstractRoute implements RouteInterface {
	const ETAPE = 'nettoyer';
	public function match() {
		# and file_exists($fichier)
		return $this->request->get('etape') === self::ETAPE;
	}

	/**
	 * @return ResponseInterface
	 */
	public function handle(
		Config $config,
		Versions $versions,
		Spip $spip,
		Page $page,
		array $contexte = []
	) {
		$response = new Response();
		$paquet = $contexte['paquet'];

		$archive = $config->get('directory.tmp') . basename($paquet);
		$chmod = $config->get('chmod');

		if (!(file_exists($archive))) {
			$page->setInner(
				'<div class="error">'
				. $page->getTranslator()->translate('tradloader:echec_chargement')
				. "<br><em>$paquet, $archive</em>"
				. '</div>'
			);
			return $response->setContent($page->toHTML())->setStatusCode(400);
		}

		$url_root = $config->get('url.root');
		$dir_install = $config->get('directory.install');
		$url = $url_root . $dir_install;
		$dir = ($dir_install ?: './');

		$session = new Session();
		if ($session->has('redirect_path')) {
			$url .= $session->get('redirect_path');
		} else {
			$url .= $config->get('redirect.path');
		}

		$cleaner = new Cleaner(
			($dir_install ?: './'),
			$chmod,
			$config->get('cleaner.keep.directory')
		);
		$cleaner->nettoyer_superflus($archive, $dir);
		$cleaner->nettoyer_racine($archive);
		$this->creer_repertoires_plugins($dir_install, $chmod);

		$session = new Session();
		$session->clean();

		// Afficher 100% et rediriger sur SPIP !
		$title = $page->getTranslator()->translate('etape_fin');
		return $response->setContent($page->redirige_boucle($url, $title));
	}

	/**
	 * creer repertoire
	 *
	 * @param string $dir_install
	 * @param int $chmod
	 * @return void
	 */
	public function creer_repertoires_plugins($dir_install, $chmod) {
		// créer les répertoires plugins/auto et lib
		if (!is_dir($dir_install . 'plugins')) {
			@mkdir($dir_install . 'plugins', $chmod);
		}
		if (!is_dir($dir_install . 'plugins/auto')) {
			@mkdir($dir_install . 'plugins/auto', $chmod);
		}
		if (!is_dir($dir_install . 'lib')) {
			@mkdir($dir_install . 'lib', $chmod);
		}
	}
}
