<?php

namespace Spip\Loader\Route;

use Spip\Loader\Http\RequestInterface;

abstract class AbstractRoute
{
	/** @var RequestInterface */
	protected $request;

	public function __construct(RequestInterface $request) {
		$this->request = $request;
	}
}
