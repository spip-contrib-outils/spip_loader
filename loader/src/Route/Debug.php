<?php

namespace Spip\Loader\Route;

use Spip\Loader\Api\Versions;
use Spip\Loader\Config\Config;
use Spip\Loader\Filesystem;
use Spip\Loader\Http\Response;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\Remote;
use Spip\Loader\Route\RouteInterface;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

class Debug extends AbstractRoute implements RouteInterface {
	const ETAPE = 'debug';

	public function match() {
		return $this->request->get('etape') === self::ETAPE;
	}

	/**
	 * {@inheritDoc}
	 */
	public function handle(Config $config, Versions $versions, Spip $spip, Page $page, array $contexte = []) {
		if (!$config->get('debug')) {
			throw new \Exception('Debug config is not enabled');
		}

		$corps = '<div class="msg-alert info debug">';
		$corps .= 'Ces informations peuvent aider à comprendre certains problèmes rencontrés.';
		$corps .= '</div>';

		$corps .= $this->getTable('Context', $contexte);
		$corps .= $this->getTable('Server', [
			'__FILE__' => __FILE__,
			'ORIG_PATH_INFO' => $this->request->server('ORIG_PATH_INFO'),
			'PATH_INFO' => $this->request->server('PATH_INFO'),
			'SCRIPT_NAME' => $this->request->server('SCRIPT_NAME'),
			'SCRIPT_FILENAME' => $this->request->server('SCRIPT_FILENAME'),
			'HTTP_HOST' => $this->request->server('HTTP_HOST'),
			'PHP_SELF' => $this->request->server('PHP_SELF'),
		]);
		$corps .= $this->getTable('Phar', [
			'Phar::running' => \Phar::running(),
			'Phar::running(false)' => \Phar::running(false),
		]);
		$corps .= $this->getTable('Config', $config->toArray());

		$page->setTitle('Debug');
		$page->setInner($corps);
		$response = new Response();
		return $response->setContent($page->toHTML());
	}

	/**
	 * @param string $title
	 * @param array<string, mixed> $values
	 * @return string
	 */
	private function getTable($title, array $values) {
		$table = '<table class="table--debug">'
			. '<caption>' . $title . '</caption>'
			. '<thead><tr><th>Key</th><th>Value</th></tr></thead>'
			. '<tbody>';
		foreach ($values as $key => $value) {
			$table .= sprintf('<tr><th>%s</th><td>%s</td></tr>', $key, var_export($value, true));
		}
		$table .= '</tbody>'
			. '</table>';
		return $table;
	}
}
