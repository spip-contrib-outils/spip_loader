<?php

namespace Spip\Loader;

class Filesystem {
	/**
	 * Invalidates a PHP file from any active opcode caches.
	 *
	 * If the opcode cache does not support the invalidation of individual files,
	 * the entire cache will be flushed.
	 * kudo : http://cgit.drupalcode.org/drupal/commit/?id=be97f50
	 *
	 * @param string $filepath The absolute path of the PHP file to invalidate.
	 * @return void
	 */
	public static function clear_opcode_cache($filepath) {
		clearstatcache(true, $filepath);

		// Zend OPcache
		if (function_exists('opcache_invalidate')) {
			@opcache_invalidate($filepath, true);
		}

		// APC.
		if (function_exists('apc_delete_file')) {
			// apc_delete_file() throws a PHP warning in case the specified file was
			// not compiled yet.
			// @see http://php.net/apc-delete-file
			@apc_delete_file($filepath);
		}
	}

	/**
	 * @param string $src
	 * @param string $dest
	 * @param int $chmod
	 * @return void
	 */
	public static function move_all($src, $dest, $chmod) {
		$dest = rtrim($dest, '/');

		if (!is_dir($src)) {
			return;
		}

		if ($dh = opendir($src)) {
			while (($file = readdir($dh)) !== false) {
				if (in_array($file, ['.', '..'])) {
					continue;
				}
				$s = "$src/$file";
				$d = "$dest/$file";
				if (is_dir($s)) {
					if (!is_dir($d)) {
						if (!mkdir($d, $chmod, true)) {
							die("impossible de creer $d");
						}
					}
					self::move_all($s, $d, $chmod);
					rmdir($s);
					// verifier qu'on en a pas oublie (arrive parfois il semblerait ...)
					// si cela arrive, on fait un clearstatcache, et on recommence un move all...
					if (is_dir($s)) {
						clearstatcache();
						self::clear_opcode_cache($s);
						self::move_all($s, $d, $chmod);
						rmdir($s);
					}
				} else {
					if (is_file($s)) {
						rename($s, $d);
						self::clear_opcode_cache($d);
					}
				}
			}
			// liberer le pointeur sinon windows ne permet pas le rmdir eventuel
			closedir($dh);
		}
	}
}
