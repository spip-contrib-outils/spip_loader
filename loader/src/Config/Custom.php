<?php

namespace Spip\Loader\Config;

final class Custom extends AbstractConfig {
	/** @var array<mixed> */
	protected $config = [
		/**
		 * liste d’id_auteur (table spip_auteurs).
		 */
		'authorized.users' => [1],

		/**
		 * Autorise l’accès à la route 'debug'
		 * (si identifié au SPIP installé)
		 */
		'debug' => false,

		/**
		 * Chemin depuis spip_loader.php
		 * où on écrit des fichiers temporaires (spip_loader_list.json, dézippage temporaire de l’archive, ...)
		 * par défaut '' (au meme endroit).
		 * exemple 'tmp/'
		 */
		'directory.tmp' => '',

		# Quelques urls qui peuvent être surchargés (à des fins de tests)
		'url.spip.depot' => 'https://files.spip.net/',
		'url.spip_loader.api' => 'https://www.spip.net/spip_loader.api',
		'url.spip_loader.php' => 'https://get.spip.net/spip_loader.php',
		'url.spip_loader.version' => 'https://get.spip.net/version',

		# Chmod à appliquer
		'chmod' => 0755,

		# le Cleaner ne place pas dans le répertoire obsolète
		# un répertoire qui contiendrait un fichier avec ce nom.
		'cleaner.keep.directory' => '.spip_loader_keep',

		# Si définit, utilisera le chemin zip indiqué,
		# Relatif à url.zip.depot
		'archive.zip.path' => '',

		## Si définit, les appels distants sont effectués via le proxy
		'url.spip_loader.proxy' => '',

		/**
		 * Chemin depuis spip_loader.php
		 * dans quel répertoire sera dézippé l’archive
		 * (et testé la présence de SPIP et l’authentification)
		 * par défaut '' (au meme endroit que spip_loader.php).
		 *
		 * Évite de polluer le répertoire loader/public/ lors du dev de spip_loader notamment.
		 *
		 * FIXME: Problème d’authentification au SPIP si différent de ''
		 *
		 * @internal
		 * @example 'spip/'
		 */
		'directory.install' => '',
	];
}
