<?php

namespace Spip\Loader\Config;

abstract class AbstractConfig implements ConfigInterface {
	/** @var array<mixed> à remplir pour les sous classes */
	protected $config = [];

	/**
	 * @param string $filepath Path of a config file (outside the phar)
	 * @return $this
	 */
	public function loadIfExists($filepath) {
		if (!file_exists($filepath)) {
			return $this;
		}
		$override = require $filepath;
		if (!is_array($override)) {
			return $this;
		}
		foreach ($override as $key => $value) {
			$this->set($key, $value);
		}
		return $this;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 */
	public function set($key, $value) {
		if (!$this->has($key)) {
			throw new \RuntimeException(sprintf('Unknown config <code>%s</code>', $key));
		}
		$this->config[$key] = $value;
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function has($key) {
		return array_key_exists($key, $this->config);
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get($key) {
		if ($this->has($key)) {
			return $this->config[$key];
		}
		throw new \RuntimeException(sprintf('Unknown config <code>%s</code>', $key));
	}

	/** @return array<mixed> */
	public function toArray() {
		return $this->config;
	}
}
