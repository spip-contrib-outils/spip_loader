<?php

namespace Spip\Loader\Config;

interface ConfigInterface {
	/**
	 * @param string $key
	 * @param string $value
	 *
	 * @return void
	 */
	public function set($key, $value);

	/**
	 * @param string $key
	 *
	 * @return boolean
	 */
	public function has($key);

	/**
	 * @param string $key
	 *
	 * @return mixed
	 */
	public function get($key);
}
