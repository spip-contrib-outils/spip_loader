<?php

namespace Spip\Loader\Config;

use Spip\Loader\Http\RequestInterface;

final class Internal extends AbstractConfig {
	/** @var array<mixed> */
	protected $config = [
		# File that runs the application into the phar (or outside)
		# always index.php
		# - inside phar: index.php
		# - outside phar: index.php
		'app.filename' => '',

		# Phar (only) file name
		# - inside phar: spip_loader.php
		# - outside phar: ''
		'phar.filename' => '',

		# Path of source code directory (phar:// or /path/to/directory/)
		# We can load internal phar files with that
		# - inside phar: phar://spip_loader.phar
		# - outside phar: /[..]/opt/build/loader
		'directory.root' => 'phar://spip_loader.phar',

		# Absolute path of the public directory (always absolute without phar:///)
		# We can load external files (outside the phar) with that
		# - inside phar: /[...]/opt/spip_loader/build
		# - outside phar: /[...]/opt/spip_loader/loader/public
		'directory.public' => '',

		# url of the main application (spip_loader) directory
		# (may be a phar spip_loader.php or an index.php outside)
		# - inside phar: //domain.tld/path/spip_loader.php/
		# - outside phar: //domain.tld/path/
		'url.frontend' => '',

		# url of the main site (spip_loader) directory
		# where script phar is executed
		# - inside phar: //domain.tld/path/
		# - outside phar: //domain.tld/path/
		'url.root' => '',

		'archive.zip.name' => 'spip',

		'redirect.path' => 'ecrire/?exec=install',
		'redirect.installed.path' => 'ecrire/?exec=accueil&var_mode=calcul',
	];

	/**
	 * Phar build adds '/index.php' in url that can be annoying.
	 * We save 'script_url' to send a <base href="xxx"> information to the browser.
	 *
	 * @note
	 *    SCRIPT_FILENAME  : Local path of the primary script
	 *    - inside phar : /[...]/opt/spip_loader/build/spip_loader.php
	 *    - outside phar : /[...]/opt/spip_loader/loader/public/index.php
	 *
	 *   $script_url Url of the spip_loader.php script
	 *   - inside phar : //loader.spip.test/build/spip_loader.php
	 *   - outside phar : //loader.spip.test/loader/public/index.php
	 * @param bool $is_cli
	 */
	public function __construct(RequestInterface $request, $is_cli = false) {
		$script_filename = $request->server('SCRIPT_FILENAME');
		$script_url = '';
		if (!$is_cli) {
			$script_name = $request->server('SCRIPT_NAME');
			$path = $request->server('PATH_INFO');
			if (!$path) {
				$path = (string) substr(
					parse_url($request->server('REQUEST_URI'), PHP_URL_PATH),
					strlen($request->server('PHP_SELF'))
				);
			}
			$self = $script_name . $path;
			$script_url = "//{$request->server('HTTP_HOST')}$self";
		}

		$this->set('app.filename', basename($script_url));
		$this->set('phar.filename', basename(\Phar::running(false)));
		$this->set('url.frontend', dirname($script_url) . '/');
		if (! \Phar::running(false)) {
			$this->set('directory.root', dirname(dirname(__DIR__)));
		}
		$this->set('directory.public', dirname($script_filename));
		$this->set('url.root', dirname($script_url) . '/');
		# $this->set('url.root', dirname($script_url, \Phar::running(false) ? 2 : 1) . '/'); # php 7.0+
		#$this->set('url.root', (\Phar::running(false) ? dirname(dirname($script_url)) : dirname($script_url)) . '/');
	}
}
