<?php

namespace Spip\Loader\Config;

final class Config extends AbstractConfig {
	public function __construct(Internal $internal, Custom $custom) {
		$this->config = array_merge($internal->toArray(), $custom->toArray());
	}
}
