<?php

namespace Spip\Loader\Template;

use Spip\Loader\Config\Config;
use Spip\Loader\I18n\Translator;

class Page extends Template {
	const PAGE_TYPE = 'admin';

	/** @var Translator */
	protected $translator;

	/** @var Config */
	protected $config;

	public function __construct(
		Translator $translator,
		Config $config
	) {
		parent::__construct();
		$this->translator = $translator;
		$this->config = $config;
		$this->setLang($this->translator->getLang());
		$this->setBaseUri($config->get('phar.filename'));
		$this->setFrontendUrl($config->get('url.frontend'));
		$this->setAppFilename($config->get('app.filename'));
	}

	/** @return Translator */
	public function getTranslator() {
		return $this->translator;
	}

	/** @return Config */
	public function getConfig() {
		return $this->config;
	}
}
