<?php

namespace Spip\Loader\Template;

class Error extends Template {
	const PAGE_TYPE = 'error';

	/** @var string */
	private $message;

	/**
	 * @param string $title
	 * @param string $message
	 */
	public function __construct($title, $message) {
		$this->setTitle($title);
		$this->message = $message;
		$this->setInner('
			<span class="loader">{app_name}</span>
			<div class="error">
				<p>{message}</p>
			</div>
		');
	}

	/** @return self */
	public static function fromException(\Exception $e) {
		return new self('Error', $e->getMessage());
	}

	/** @return array<mixed> */
	protected function context() {
		return array_merge(
			parent::context(),
			[
				'message' => $this->message
			]
		);
	}
}
