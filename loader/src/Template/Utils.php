<?php

namespace Spip\Loader\Template;

use Spip\Loader\I18n\Languages;
use Spip\Loader\Config\Config;
use Spip\Loader\Api\Versions;

class Utils {
	/**
	 * @return array<mixed>
	 */
	public static function http_no_cache() {
		return [
			'Content-Type' => 'text/html; charset=utf-8',
			'Expires' => '0',
			'Last-Modified' => '' . gmdate('D, d M Y H:i:s') . ' GMT',
			'Cache-Control' => 'no-cache, must-revalidate',
			'Pragma' => 'no-cache',
		];
	}

	/**
	 * Produire le menu de langue.
	 *
	 * @param string $lang
	 * @param string $script
	 * @param array<mixed> $hidden
	 * @return string
	 */
	public static function menu_langues($lang, $script = '', $hidden = []) {
		unset($hidden['etape']);
		$script .= '?' . ($hidden ? http_build_query($hidden, '', '&amp;') . '&amp;' : '');
		$menu = <<<HTML
			<div class="menu_lang">
				<select name="lang" onchange="window.location='{$script}lang='+this.value;">
HTML;
		foreach (Languages::LANGUAGES as $l => $nom) {
			$selected = ($l == $lang ? ' selected="selected"' : '');
			$menu .= '<option value="' . $l . '"' . $selected . '>' . $nom . "</option>\n";
		}
		$menu .= <<<HTML
			</select>
			<noscript>
				<div><input type="submit" name="ok" value="ok" /></div>
			</noscript>
		</div>
HTML;
		return $menu;
	}

	/**
	 * Affiche un sélecteur de menu pour choisir le zip (la branche) à utiliser.
	 *
	 * @param Config $config
	 * @param Versions $versions
	 * @param string $active
	 * @param string $version_installee
	 * @param bool $presente
	 * @return string
	 */
	public static function menu_branches(
		Config $config,
		Versions $versions,
		$active,
		$version_installee,
		$presente
	) {

		$script = $config->get('app.filename');
		$select = '';
		if (!defined('_CHEMIN_FICHIER_ZIP_FORCEE')) {
			$script .= '?';
			$select .= "<div class='menu_branches'>";
			$select .= '<select name="chemin" onchange="window.location=\'' . $script . 'chemin=\'+this.value;">';

			foreach ($versions->lister_branches_proposees() ?: [] as $branche => $desc) {
				if ($branche == 'dev' or !$version_installee or version_compare($versions->branche_spip($version_installee), $branche, '<=')) {
					$_active = ($active == $desc['zip']) && ($presente);
					$select .= '<option value="' . $desc['zip'] . '"' . ($_active ? ' selected="selected"' : '') . '>'
						. 'SPIP ' . $branche
						. "</option>\n";
				}
			}
			if (!$presente) {
				$select .= '<option value="" selected="selected">'
				. 'Sélectionnez...'
				. "</option>\n";
			}
			$select .= '</select> <noscript><div><input type="submit" name="ok" value="ok" /></div></noscript>';
			$select .= '</div>';
		}
		return $select;
	}
}
