<?php

namespace Spip\Loader\Template;

use Spip\Loader\I18n\Languages;

class Template {
	const APP_NAME = 'Spip Loader';

	const PAGE_TYPE = 'public';

	/** @var string */
	private $title;

	/** @var string */
	private $lang = 'fr';

	/** @var string */
	private $base_uri = '.'; // spip_loader.php when in phar

	/** @var string */
	private $frontend_url = ''; // url of the script directory

	/** @var string */
	private $app_filename = '.'; // index.php ou spip_loader.php when in phar

	/** @var string */
	private $app_version = '';

	/** @var array<string> */
	private $css_files = [
		'minipage.vars.css',
		'reset.css',
		'clear.css',
		'minipage.css',
		'minipres.css',
		'app.css',
	];

	/** @var string */
	private $css_compile_file = 'all.css';

	/** @var string */
	private $inner = '';

	/** @var array<mixed> */
	private $hiddens = [];

	/** @var string */
	private $menu_lang = '';

	public function __construct() {
		$this->setTitle(self::APP_NAME);
	}

	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @param string $lang
	 * @return void
	 */
	public function setLang($lang) {
		$this->lang = $lang;
	}

	/**
	 * @param string $base_uri
	 * @return void
	 */
	public function setBaseUri($base_uri) {
		$this->base_uri = $base_uri ?: '.';
	}

	/**
	 * @param string $frontend_url
	 * @return void
	 */
	public function setFrontendUrl($frontend_url) {
		$this->frontend_url = $frontend_url ?: '';
	}

	/**
	 * @param string $app_filename
	 * @return void
	 */
	public function setAppFilename($app_filename) {
		$this->app_filename = $app_filename;
	}

	/**
	 * @param string $version
	 * @return void
	 */
	public function setAppVersion($version) {
		$this->app_version = $version;
	}

	/** @return array<mixed> */
	protected function context() {
		return [
			'app_name' => self::APP_NAME,
			'app.version' => $this->app_version,
			'app.filename' => $this->app_filename,

			'title' => $this->title,
			'page_type' => self::PAGE_TYPE,
			'uri.base' => $this->base_uri,
			'url.frontend' => $this->frontend_url,

			'lang' => $this->lang,
			'lang_dir' => Languages::getLangDir($this->lang),
			'lang_right' => Languages::getLangRight($this->lang),
			'lang_left' => Languages::getLangLeft($this->lang),

			'menu_lang' => $this->menu_lang,
		];
	}

	/** @return string */
	public function toHTML() {
		return $this->render($this->page(), $this->context());
	}

	/** @return string */
	protected function color_base() {
		return <<<HTML
			<style>
			:root {
				--minipage-color-theme--h: 0;
				--minipage-color-theme--s: 0%;
				--minipage-color-theme--l: 60%;
			}
			</style>
HTML;
	}

	/**
	 * @param string $file
	 * @return string
	 */
	protected function assets_path($file) {
		return (\Phar::running(false) ? $this->app_filename . '?file=' : './') . sprintf('assets/%s', $file);
	}

	/** @return string */
	protected function css_files() {
		// Dans le phar, les CSS sont compilées dans un fichier unique.
		if (file_exists(__DIR__ . '/../../public/assets/' . $this->css_compile_file)) {
			return sprintf('<link rel="stylesheet" href="%s" type="text/css">', $this->assets_path($this->css_compile_file)) . "\n";
		}
		// Sinon chacunes visibles
		$css = '';
		foreach ($this->css_files as $file) {
			$css .= sprintf('<link rel="stylesheet" href="%s" type="text/css">', $this->assets_path($file)) . "\n";
		}
		return $css;
	}

	/** @return string */
	protected function css() {
		return $this->color_base() . $this->css_files();
	}

	/** @return string */
	protected function head() {
		return <<<HTML
<title>{app_name} — {title}</title>
<base href="{url.frontend}" />
<meta name="viewport" content="width=device-width" />
<link rel="shortcut icon" href="{$this->assets_path('spip_loader.ico')}" type="image/x-icon">
{$this->css()}
HTML;
	}

	/** @return string */
	protected function header() {
		// nom site | url ?
		return <<<HTML
		<h1>{title}</h1>
		{menu_lang}
HTML;
	}


	/** @return string */
	public function inner() {
		return $this->inner;
	}

	/**
	 * @param string $html
	 * @return void
	 */
	public function setInner($html) {
		$this->inner = $html;
	}

	/**
	 * @param array<mixed> $hiddens
	 * @return void
	 */
	public function setHiddens(array $hiddens = []) {
		$this->hiddens = $hiddens;
	}

	/**
	 * @return string
	 */
	protected function hiddens() {
		$_hiddens = '';
		foreach ($this->hiddens as $k => $v) {
			$_hiddens .= "<input type='hidden' name='$k' value='$v' />\n";
		}
		return $_hiddens;
	}

	/** @return string */
	protected function footer() {
		return <<<HTML
	<div class="app-footer">
		<span class="app-logo"><img src="{$this->assets_path('spip_loader.svg')}" alt="{app_name}" /></span>
		<strong class="app-version">{app.version}</strong>
	</div>
HTML;
	}

	/** @return string */
	protected function page() {
		return $this->pageStart() . $this->inner() . $this->pageEnd();
	}

	/** @return string */
	protected function pageStart() {
		return $this->ouvreBody() . $this->ouvreCorps();
	}

	/** @return string */
	protected function pageEnd() {
		return $this->fermeCorps() . $this->fermeBody();
	}

	/** @return string */
	protected function ouvreBody() {
		return <<<HTML
<!DOCTYPE html>
<html class='{lang_dir} {lang} no-js' xmlns='http://www.w3.org/1999/xhtml' lang='{lang}' dir='{lang_dir}'>
	<head>
		{$this->head()}
	</head>
	<body class="minipage minipage--{page_type}">
		<div class="minipage-bloc">
			<div class="page">
				<form action="{app.filename}" method='get'>
					{$this->hiddens()}
HTML;
	}

	/** @return string */
	protected function ouvreCorps() {
		return <<<HTML
					<header>
						{$this->header()}
					</header>
					<div class='corps'>
HTML;
	}

	/** @return string */
	protected function fermeCorps() {

		return <<<HTML
					</div>
					<footer>
					{$this->footer()}
					</footer>
HTML;
	}

	/** @return string */
	protected function fermeBody() {
		return <<<HTML
			</form>
		</div>
	</body>
</html>
HTML;
	}

	/** @return void */
	public function withLangMenu() {
		$this->menu_lang = Utils::menu_langues($this->lang, $this->app_filename, $this->hiddens);
	}

	/**
	 * @param string $url
	 * @param string $title
	 * @return string
	 */
	public function redirige_boucle($url, $title = '') {
		@ini_set('zlib.output_compression', '0'); // pour permettre l'affichage au fur et a mesure
		@ini_set('output_buffering', 'off');
		@ini_set('implicit_flush', 1);
		@ob_implicit_flush(true);
		$corps = '<meta http-equiv="refresh" content="0;' . $url . '">';
		if ($title) {
			$corps .= "<h2>$title</h2>";
		}
		$corps .= <<<HTML
			<div class="waiting">
				<img src="{$this->assets_path('loader.svg')}" />
			</div>
HTML;
		$this->setInner($corps);
		return $this->toHTML();
	}


	/**
	 * @param string $templateContent
	 * @param array<mixed> $context
	 * @return string
	 */
	protected function render($templateContent, array $context = []) {
		$keys = [];
		foreach (array_keys($context) as $key) {
			$keys[] = '{' . $key . '}';
		}

		return str_replace($keys, $context, $templateContent);
	}
}
