<?php

namespace Spip\Loader;

use Spip\Loader\Config\Internal;
use Spip\Loader\Http\Request;

final class Debug {
	/** @var bool */
	private static $enable = false;

	/** @var array<string, mixed> */
	private static $bag = [];

	/** @var Internal */
	private $internal;

	/** @var Request */
	private $request;

	public function __construct(Request $request, Internal $internal) {
		$this->request = $request;
		$this->internal = $internal;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return void;
	 */
	public static function add($key, $value) {
		self::$bag[$key] = $value;
	}

	/** @return void */
	public static function enable() {
		if (!self::$enable) {
			self::$enable = true;
			@ini_set('display_errors', 1);
			error_reporting(E_ALL ^ E_DEPRECATED);
			register_shutdown_function(function () {
				$request = Request::createFromGlobals();
				$debug = new Debug($request, new Internal($request));
				echo $debug->debug();
			});
		}
	}

	/** @return string */
	public function debug() {
		return "<div class='msg-alert notice' style='margin-bottom:0'>"
		. $this->table($this->internal->toArray())
		. (self::$bag ? $this->table(self::$bag) : '')
		. $this->table($this->server_info())
			. '</div>';
	}

	/** @return array<string, scalar> */
	private function server_info() {
		return [
			'PATH_INFO' => $this->request->server('PATH_INFO'),
			'REQUEST_URI' => $this->request->server('REQUEST_URI'),
			'SCRIPT_FILENAME' => $this->request->server('SCRIPT_FILENAME'),
			'SCRIPT_NAME' => $this->request->server('SCRIPT_NAME'),
			'PHP_SELF' => $this->request->server('PHP_SELF'),
			'HTTP_HOST' => $this->request->server('HTTP_HOST'),
			'Phar::running(false)' => \Phar::running(false),
			'PHP Version' => phpversion(),
			'ZipArchive' => class_exists('ZipArchive'),
		];
	}

	/**
	 * @param array<string, mixed> $data
	 * @return string
	 */
	private function table(array $data) {
		$box = "<table style='margin: .75em 0;'>";
		foreach ($data as $key => $val) {
			$box .= "<tr><th>$key</th><td><code>" . var_export($val, true) . "</code></td></tr>\n";
		}
		$box .= "</table>\n";
		return $box;
	}
}
