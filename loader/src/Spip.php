<?php

namespace Spip\Loader;

class Spip {
	const BOOTSTRAP_SPIP_FILE = 'ecrire/inc_version.php';

	/** @var bool */
	private $loaded = false;

	/** @var string realpath of install directory */
	private $full_install_directory;

	/** @var string install directory relative to working directory */
	private $install_directory;

	/** @var string */
	private $url_install;

	/** @var string */
	private $script_filename;

	/**
	 * @param string $url_install
	 * @param string $full_install_directory
	 * @param string $install_directory
	 * @param string $script_filename
	 */
	public function __construct($url_install, $full_install_directory, $install_directory, $script_filename) {
		$this->url_install = $url_install;
		$this->full_install_directory = $full_install_directory;
		$this->install_directory = $install_directory;
		$this->script_filename = $script_filename;
	}

	/**
	 * @return bool
	 */
	public function exists() {
		return @file_exists($this->full_install_directory . self::BOOTSTRAP_SPIP_FILE);
	}

	/** @return string */
	public function version_installee() {
		return $this->getSpipVar('spip_version_branche');
	}

	/**
	 * @param string $var
	 * @return string
	 * */
	protected function getSpipVar($var) {
		if (!$this->exists()) {
			return '';
		}
		$file = $this->full_install_directory . self::BOOTSTRAP_SPIP_FILE;
		if (!is_readable($file)) {
			throw new \Exception(sprintf('Can’t read %s to extract version', $file));
		}
		return $this->getVarValue(file_get_contents($file), $var);
	}

	/**
	 * cherche la valeur de $truc = valeur; dans un contenu
	 *
	 * @param string $content
	 * @param string $var
	 * @return string
	 **/
	protected function getVarValue($content, $var) {
		$expr = '/^\$' . $var . '\s?=(.+);$/m';
		if (!\preg_match($expr, $content, $m)) {
			return '';
		}
		return trim($m[1], "'\" ");
	}

	/**
	 * @return void
	 */
	public function start() {
		if ($this->loaded) {
			return;
		}
		$this->loaded = true;
		if (self::exists()) {
		# Déclarer les globales utilisées encore par SPIP :/
		global
			$nombre_de_logs,
			$taille_des_logs,
			$table_prefix,
			$cookie_prefix,
			$dossier_squelettes,
			$filtrer_javascript,
			$type_urls,
			$debut_date_publication,
			$ip,
			$mysql_rappel_connexion,
			$mysql_rappel_nom_base,
			$test_i18n,
			$ignore_auth_http,
			$ignore_remote_user,
			$derniere_modif_invalide,
			$quota_cache,
			$home_server,
			$help_server,
			$url_glossaire_externe,
			$tex_server,
			$traiter_math,
			$xhtml,
			$xml_indent,
			$source_vignettes,
			$formats_logos,
			$controler_dates_rss,
			$spip_pipeline,
			$spip_matrice,
			$plugins,
			$surcharges,
			$exceptions_des_tables,
			$tables_principales,
			$table_des_tables,
			$tables_auxiliaires,
			$table_primary,
			$table_date,
			$table_titre,
			$tables_jointures,
			$liste_des_statuts,
			$liste_des_etats,
			$liste_des_authentifications,
			$spip_version_branche,
			$spip_version_code,
			$spip_version_base,
			$spip_sql_version,
			$spip_version_affichee,
			$visiteur_session,
			$auteur_session,
			$connect_statut,
			$connect_toutes_rubriques,
			$hash_recherche,
			$hash_recherche_strict,
			$ldap_present,
			$meta,
			$connect_id_rubrique,
			$puce;

			$pwd = getcwd();
			chdir($this->full_install_directory);
			// Charger l'autoload de SPIP
			$spip_autoload = $this->full_install_directory . '/vendor/autoload.php';
			if (\file_exists($spip_autoload)) {
				require_once $spip_autoload;
			}
			include_once self::BOOTSTRAP_SPIP_FILE;
			chdir($pwd);
		}
	}

	/** @return bool */
	public function isUsable() {
		$this->start();
		if (
			defined('_FILE_CONNECT') and constant('_FILE_CONNECT') and strpos(constant('_FILE_CONNECT'), '.php')
		) {
			return true;
		}
		if (defined('_SITES_ADMIN_MUTUALISATION')) {
			return true;
		}
		return false;
	}

	/**
	 * @param array<mixed> $authors
	 * @return bool
	 */
	public function isAuthenticated(array $authors) {
		$this->start();

		if (
			isset($GLOBALS['auteur_session']['statut'])
			&& $GLOBALS['auteur_session']['statut'] === '0minirezo'
			&& in_array($GLOBALS['auteur_session']['id_auteur'], $authors)
		) {
			return true;
		}
		return false;
	}

	/**
	 * @return void
	 */
	public function askForLogin() {

		$return = str_repeat('../', $this->countDirectories($this->install_directory));
		$redirect_self = $return . $this->script_filename;
		$pwd = getcwd();
		chdir($this->full_install_directory);
		include_spip('inc/headers');
		include_spip('inc/minipres');
		http_response_code(403);
		echo str_replace('<head>', '<head><base href="' . $this->url_install . '">', install_debut_html(_T('info_acces_interdit')));
		echo "<div style='text-align: center'>\n";
		echo _T('ecrire:avis_non_acces_page');
		echo '<br /><a href="' .  parametre_url(generer_url_public('login'), 'url', $redirect_self) . '">' . _T('public:lien_connecter') . '</a>';
		echo "\n</div>";
		echo install_fin_html();
		chdir($pwd);
	}

	/**
	 * @param string $path
	 * @return int
	 */
	private function countDirectories($path) {
		$path = trim($path, '/');
		if ($path) {
			return 1 + substr_count($path, '/');
		}
		return 0;
	}
}
