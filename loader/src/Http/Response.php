<?php

namespace Spip\Loader\Http;

use function headers_sent;
use function header;
use function http_response_code;

class Response implements ResponseInterface
{
	/** @var int */
	private $status_code;

	/** @var array<string, string> */
	private $headers;

	/** @var string */
	private $content;

	/**
	 * @param string $content
	 * @param integer $status_code
	 * @param array<string, string> $headers
	 */
	public function __construct($content = '', $status_code = 200, $headers = []) {
		$this->content = $content;
		$this->status_code = $status_code;
		$this->headers = $headers;
	}

	/**
	 * {@inheritDoc}
	 */
	public function send() {
		if (!headers_sent()) {
			foreach ($this->headers as $key => $value) {
				header($key . ': ' . $value);
			}
		}

		http_response_code($this->status_code);

		echo $this->content;

		return $this;
	}

	/** @return string */
	public function getContent() {
		return $this->content;
	}

	/**
	 * @param string $content
	 * @return $this
	 */
	public function setContent($content) {
		$this->content = $content;

		return $this;
	}

	/**
	 * @param int $status_code
	 * @return $this
	 */
	public function setStatusCode($status_code) {
		$this->status_code = $status_code;

		return $this;
	}
}
