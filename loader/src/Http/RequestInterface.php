<?php

namespace Spip\Loader\Http;

interface RequestInterface
{
	/**
	 * @param string $param
	 * @return string|null
	 */
	public function get($param);

	/**
	 * @param string $param
	 * @return string|null
	 */
	public function server($param);

	/**
	 * @param string $param
	 * @return string|null
	 */
	public function getCookie($param);
}
