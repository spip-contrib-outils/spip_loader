<?php

namespace Spip\Loader\Http;

interface ResponseInterface
{
	/**
	 * @return $this
	 */
	public function send();
}
