<?php

namespace Spip\Loader\Http;

class Request implements RequestInterface
{
	/** @var array<mixed> */
	private $get;

	/** @var array<mixed> */
	private $server;

	/** @var array<mixed> */
	private $cookie;

	/** @var array<mixed> */
	private $headers;

	/**
	 * @param array<mixed> $get
	 * @param array<mixed> $server
	 * @param array<mixed> $cookie
	 * @param array<mixed> $headers
	 */
	public function __construct(
		array $get = [],
		array $server = [],
		array $cookie = [],
		array $headers = []
	) {
		$this->get = $get;
		$this->server = $server;
		$this->cookie = $cookie;
		$this->headers = $headers;
	}

	/**
	 * @return self
	 */
	public static function createFromGlobals() {
		return new self($_GET, $_SERVER, $_COOKIE);
	}

	/**
	 * {@inheritDoc}
	 */
	public function get($param) {
		return isset($this->get[$param]) ? $this->get[$param] : null;
	}

	/**
	 * {@inheritDoc}
	 */
	public function server($param) {
		return isset($this->server[$param]) ? $this->server[$param] : null;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getCookie($param) {
		return isset($this->cookie[$param]) ? $this->cookie[$param] : null;
	}

	/**
	 * @param string $param
	 * @param string $value
	 * @return void
	 */
	public function setCookie($param, $value) {
		setcookie($param, $value);
	}

	/**
	 * @param string $param
	 * @return string|null
	 */
	public function header($param) {
		return isset($this->headers[$param]) ? $this->headers[$param] : null;
	}
}
