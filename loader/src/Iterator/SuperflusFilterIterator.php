<?php

namespace Spip\Loader\Iterator;

/**
 * Iterateur des dossiers et fichiers superflus...
 * Note : utiliser CallbackFilterIterator quand PHP >= 5.4 pour SL.
 */
class SuperflusFilterIterator extends \FilterIterator
{
	/** @var array<mixed> */
	private $contenus_source;

	/** @var int */
	private $ignore_len;

	/** @var array<int,string> */
	private $keepDirs = [];

	/** @var string */
	private $keep_directory_file;

	/**
	 * @param \Iterator $iterator
	 * @param array<string,bool> $contenus_source
	 * @param string $dir
	 * @param string $keep_directory_file
	 */
	public function __construct($iterator, $contenus_source, $dir, $keep_directory_file) {
		parent::__construct($iterator);
		$this->contenus_source = $contenus_source;
		$this->ignore_len = strlen($dir);
		$this->keep_directory_file = $keep_directory_file;
	}

	#[\ReturnTypeWillChange]
	public function accept() {
		$file = $this->getInnerIterator()->current();
		if ($this->isFileInSources($file)) {
			return false;
		}
		if ($this->isSpipWrittenFile($file)) {
			return false;
		}
		if ($this->isFileInKeepDirectory($file)) {
			return false;
		}
		return true;
	}

	/**
	 * @param \SplFileInfo $file
	 * @return bool
	 */
	public function isFileInSources($file) {
		$path = (string) substr($file->getPathname(), $this->ignore_len);
		return isset($this->contenus_source[$path]);
	}

	/**
	 * @param \SplFileInfo $file
	 * @return bool
	 */
	public function isSpipWrittenFile($file) {
		return in_array($file->getFileName(), ['.ok', '.htaccess']);
	}

	/**
	 * Vérifie si un fichier ou répertoire est à l’intérieur d’un répertoire contenant .spip_loader_keep
	 * @note
	 *    On tente de ne pas mettre les fichiers et dossiers d'un dossier déjà ignoré...
	 *    ne fonctionne que si l'iterateur en entrée est bien trié (::SELF_FIRST)
	 * @param \SplFileInfo $file
	 * @return bool
	 */
	public function isFileInKeepDirectory($file) {
		if (!$this->keep_directory_file) {
			return false;
		}
		$isDirectory = $file->isDir();
		if ($isDirectory) {
			if (file_exists($file->getPathname() . '/' . $this->keep_directory_file)) {
				$this->keepDirs[] = $file->getPathname();
				return true;
			}
		}
		$parent = $file->getPath();
		if (in_array($parent, $this->keepDirs)) {
			if ($isDirectory) {
				$this->keepDirs[] = $file->getPathname();
			}
			return true;
		}
		return false;
	}
}
