<?php

namespace Spip\Loader\Api;

class Versions {
	/** @var V2 */
	private $api;

	public function __construct(V2 $api) {
		$this->api = $api;
	}

	/**
	 * Détermine quelle est la branche par défaut à utiliser.
	 *
	 * @return string|false
	 */
	public function determiner_branche_par_defaut() {
		static $default = null;

		if ($default === null) {
			$branches = $this->lister_branches_proposees();
			$liste = $this->lister_versions_spip();
			$branch = $liste['default_branch'];

			$default = isset($branches[$branch]) ? $branches[$branch] : false;
		}

		return $default;
	}


	/**
	 * Liste des versions possibles
	 * (avec l’adresse du zip et la version minimale de PHP)
	 *
	 * @param string|null $branch
	 *     Pour retourner l’info d’une branche spécifique
	 * @return array<mixed>|false
	 *     Descriptif des branches, ou d’une seule branche
	 */
	public function lister_branches_proposees($branch = null) {
		static $branches = null;

		if ($branches === null) {
			$liste = $this->lister_versions_spip();
			$branches = array_column($liste['versions'], null, 'branche');
		}
		if (!is_null($branch)) {
			return isset($branches[$branch]) ? $branches[$branch] : false;
		}
		return $branches;
	}

	/**
	 * @param string $version
	 * @return string
	 */
	public function branche_spip($version) {
		if (in_array($version, ['master', 'dev'])) {
			return 'dev';
		}
		$v = explode('.', $version);
		$branche = $v[0] . '.' . (isset($v[1]) ? $v[1] : '0');
		return $branche;
	}


	/**
	 * Renvoie une définition des versions SPIP
	 *
	 * Tableau
	 * - defaut_branch => version
	 * - versions => [ chemin => [ description de la version ]]
	 *
	 * @throws \Exception
	 *
	 * @return array<mixed>
	 */
	public function lister_versions_spip() {
		$liste = $this->api->getJson();

		$php = !empty($liste['requirements']['php']) ? $liste['requirements']['php'] : [];
		$versions = [];
		foreach ($liste['versions'] as $version => $path) {
			$branch = $this->getBranchNameFromTag($version);
			$php_min = !empty($php[$branch]) ? $php[$branch] : (!empty($php['master']) ? $php['master'] : null);
			$versions[$path] = [
				'version' => $version,
				'branche' => ($version === 'dev' ? 'dev' : $branch),
				'etat' => ($version === 'master' ? 'dev' : 'stable'),
				'zip' => $path,
				'php_min' => $php_min,
			];
		}

		return [
			'default_branch' => !empty($liste['default_branch']) ? $liste['default_branch'] : null,
			'versions' => $versions,
		];
	}

	/**
	 * Extraire la branche à partir d'un tag.
	 *
	 * @param string $tag
	 * @return string
	 */
	public function getBranchNameFromTag($tag) {
		if (!$tag) {
			return '';
		}
		if ($tag[0] === 'v') {
			$tag = substr($tag, 1);
		}
		$b = explode('.', $tag);
		$branch = [];
		$branch[] = array_shift($b);
		$branch[] = array_shift($b);
		$branch = implode('.', $branch);
		return $branch;
	}
}
