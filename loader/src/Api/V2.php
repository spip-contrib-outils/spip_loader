<?php

namespace Spip\Loader\Api;

use Spip\Loader\Config\Config;
use Spip\Loader\Debug;
use Spip\Loader\Filesystem;
use Spip\Loader\Remote;

class V2 {
	const JSON_FILE = 'spip_loader_list.json';
	const API_VERSION = 2;

	/** @var Filesystem */
	private $filesystem;

	/** @var Config */
	private $config;

	public function __construct(Filesystem $filesystem, Config $config) {
		$this->filesystem = $filesystem;
		$this->config = $config;
	}

	/** @return array<mixed> */
	public function getJson() {
		// Récupération du fichier spip_loader_list.json
		$filename = self::JSON_FILE;
		$tmpDirectory = $this->config->get('directory.tmp');
		$urlSpipLoaderApi = $this->config->get('url.spip_loader.api');
		$ttl = 10 * 60; // 10mn
		$local_spip_loader_list = $tmpDirectory . $filename;
		if (
			!file_exists($local_spip_loader_list)
			or (time() - filemtime($local_spip_loader_list) > $ttl)
		) {
			$remote = new Remote($this->config);
			$remote->copyTo($urlSpipLoaderApi, $local_spip_loader_list);
			$this->filesystem->clear_opcode_cache(realpath($local_spip_loader_list));
		}

		$liste = file_get_contents($local_spip_loader_list);
		if (!$liste) {
			throw new \Exception("Impossible de lire le fichier $filename");
		}

		$liste = json_decode($liste, true);
		if (!is_array($liste)) {
			throw new \Exception("Impossible de décoder le fichier $filename");
		}

		if (empty($liste['api'])) {
			throw new \Exception("Format de $filename inconnu");
		}

		if ($liste['api'] !== self::API_VERSION) {
			trigger_error(sprintf('Api version %s returns from %s; Expected %s.', $liste['api'], self::JSON_FILE, self::API_VERSION), \E_USER_WARNING);
			return [
				'default_branch' => null,
				'versions' => [],
			];
		}

		return $liste;
	}
}
