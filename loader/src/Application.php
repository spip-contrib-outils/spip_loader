<?php

namespace Spip\Loader;

use Spip\Loader\Api\V2 as Api;
use Spip\Loader\Api\Versions;
use Spip\Loader\Config\Config;
use Spip\Loader\Filesystem;
use Spip\Loader\Http\RequestInterface;
use Spip\Loader\Http\ResponseInterface;
use Spip\Loader\I18n\Languages;
use Spip\Loader\I18n\Translator;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

class Application {
	/** @var string */
	private $lang;

	/** @var string Version number (will be populate) */
	private $version = 'dev';

	/** @var Versions */
	private $versions;

	/** @var Config */
	private $config;

	/** @var Spip */
	private $spip;

	/** @var Page */
	private $page;

	/** @var RequestInterface */
	private $request;

	public function __construct(Config $config, RequestInterface $request) {
		$this->config = $config;
		$this->request = $request;
	}

	/**
	 * @return void
	 */
	public function run() {
		$this->init();
		if (!$this->checks()) {
			return;
		}
		$response = $this->do_run();

		$response->send();
	}

	/**
	 * @return void
	 */
	public function init() {
		$this->define_timezone();
		$this->define_version();
		$this->lang = $this->define_lang();
		$this->versions = $this->define_downloadable_versions();
		$this->spip = new Spip(
			$this->config->get('url.root') . $this->config->get('directory.install'),
			$this->config->get('directory.public') . '/' . $this->config->get('directory.install'),
			$this->config->get('directory.install'),
			$this->config->get('phar.filename') ?: $this->config->get('app.filename')
		);
		$this->page = new Template\Page(
			$this->define_translator($this->lang),
			$this->config
		);
		$this->page->setAppVersion($this->getVersion());
	}

	/** @return bool */
	protected function checks() {
		$session = new Session();
		if ($session->checked()) {
			return true;
		}
		$session->clean();

		foreach (
			[
				new Check\Authentication(),
				new Check\Filesystem($this->request),
			] as $check
		) {
			if (!$check($this->config, $this->spip, $this->page)) {
				return false;
			}
		}

		$session->create();
		# Sauver le fait qu’on était dans un SPIP qui fonctionnait
		$session->set('redirect_path', $this->config->get('redirect.path'));

		return true;
	}

	/**
	 * @return ResponseInterface
	 */
	protected function do_run() {

		$route = new Route\SelfUpdate($this->request);
		if ($route->match()) {
			return $route->handle($this->config, $this->versions, $this->spip, $this->page);
		}

		$paquet = Route\ArchiveSelection::selectArchive(
			$this->config,
			$this->versions,
			$this->spip,
			$this->request
		);

		foreach (
			[
				// Debug
				Route\Debug::class,
				// Afficher la page de presentation
				Route\ArchiveSelection::class,
				// Télécharger l’archive
				Route\ArchiveDownload::class,
				// Décompresser l'archive
				Route\ArchiveDecompress::class,
				// Nettoyages
				Route\ArchiveClean::class,
			] as $routeClass
		) {
			$route = new $routeClass($this->request);
			if ($route->match()) {
				return $route->handle($this->config, $this->versions, $this->spip, $this->page, [
					'paquet' => $paquet,
					'version' => $this->getVersion()
				]);
			}
		}

		throw new \RuntimeException('No route for that URL…');
	}

	/**
	 * @return string
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * PHP >= 5.3 rale si cette init est absente du php.ini et consorts
	 * On force a defaut de savoir anticiper l'erreur (il doit y avoir mieux)
	 *
	 * @return void
	 */
	private function define_timezone() {
		if (function_exists('date_default_timezone_set')) {
			date_default_timezone_set('Europe/Paris');
		}
	}

	/**
	 * @return void
	 */
	private function define_version() {
		if (\Phar::running()) {
			$this->version = Stub::VERSION;
		} elseif (function_exists('exec')) {
			$cmd = sprintf('/usr/bin/env git -C %s describe --tags --abbrev=0 2>/dev/null', __DIR__);
			if ($version = @exec($cmd)) {
				$this->version = $version;
			}
		}
	}

	/** @return string Code de langue */
	private function define_lang() {

		$lang = $this->request->get('lang') ?: ($this->request->getCookie('spip_lang_ecrire') ?: '');
		if (!Languages::exists($lang)) {
			$lang = Languages::find_lang_from_http_accept_language(
				$this->request->server('HTTP_ACCEPT_LANGUAGE') ?: ''
			);
		}

		# memoriser dans un cookie pour l'etape d'apres *et* pour l'install
		setcookie('spip_lang_ecrire', $lang);
		return $lang;
	}

	/**
	 * @param string $lang
	 * @return Translator
	*/
	private function define_translator($lang) {
		$translator = new Translator(
			$this->config->get('directory.root'),
			$lang,
			Languages::LANG_DEFAULT
		);
		$translator->use_language($lang);
		return $translator;
	}

	/** @return Versions */
	private function define_downloadable_versions() {
		return new Versions(
			new Api(
				new Filesystem(),
				$this->config
			)
		);
	}
}
