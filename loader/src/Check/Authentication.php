<?php

namespace Spip\Loader\Check;

use Spip\Loader\Config\Config;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

class Authentication implements CheckInterface {
	public function __invoke(Config $config, Spip $spip, Page $page) {
		if (!$spip->isUsable()) {
			return true;
		}

		$config->set('redirect.path', $config->get('redirect.installed.path'));
		if ($spip->isAuthenticated($config->get('authorized.users'))) {
			return true;
		}

		$spip->askForLogin();
		return false;
	}
}
