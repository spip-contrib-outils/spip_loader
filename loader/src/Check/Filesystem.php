<?php

namespace Spip\Loader\Check;

use Spip\Loader\Config\Config;
use Spip\Loader\Http\RequestInterface;
use Spip\Loader\Http\Response;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;
use Spip\Loader\Template\Utils;

class Filesystem implements CheckInterface {
	/** @var RequestInterface */
	private $request;

	public function __construct(RequestInterface $request) {
		$this->request = $request;
	}

	public function __invoke(Config $config, Spip $spip, Page $page) {
		if (!$this->check_working_directory($config, $page)) {
			return false;
		}

		if (!$this->check_tmp_directory($config, $page)) {
			return false;
		}

		if (!$this->check_install_directory($config, $page)) {
			return false;
		}

		return true;
	}

	/**
	 * @param Config $config
	 * @param Page $page
	 *
	 * @return boolean
	 */
	public function check_working_directory(Config $config, Page $page) {
		$chmod = $config->get('chmod');
		$script = $config->get('phar.filename') ?: $config->get('app.filename');
		$droits = $this->tester_repertoire($script, $chmod);
		if (!$droits) {
			$q = $this->request->server('QUERY_STRING');
			$page->setInner(
				$page->getTranslator()->translate(
					'tradloader:texte_preliminaire',
					[
						'paquet' => strtoupper($config->get('archive.zip.name')),
						'href'   => $config->get('app.filename') . ($q ? "?$q" : ''),
						'chmod'  => sprintf('%04o', $chmod)
					]
				)
			);
			$response = new Response($page->toHTML(), 400, Utils::http_no_cache());
			$response->send();

			return false;
		}

		return true;
	}

	/**
	 * @param Config $config
	 * @param Page $page
	 *
	 * @throws \Exception
	 * @throws \RuntimeException
	 *
	 * @return boolean
	 */
	public function check_tmp_directory(Config $config, Page $page) {
		$public_directory = $config->get('directory.public');
		$install_tmp = $config->get('directory.tmp');
		if ($install_tmp) {
			if (strpos($install_tmp, '..') !== false) {
				throw new \Exception(sprintf("directory.tmp '%s' can’t use ..", $install_tmp));
			}
			$working_install = $public_directory . '/' . $install_tmp;
			if (!file_exists($working_install)) {
				@mkdir($working_install, $config->get('chmod'), true);
			}
			if (!file_exists($working_install)) {
				throw new \RuntimeException(sprintf('Impossible de créer le répertoire: %s', $install_tmp));
			}
		}
		return true;
	}

	/**
	 * @param Config $config
	 * @param Page $page
	 *
	 * @throws \Exception
	 * @throws \RuntimeException
	 *
	 * @return boolean
	 */
	public function check_install_directory(Config $config, Page $page) {
		$public_directory = $config->get('directory.public');
		$install_dir = $config->get('directory.install');
		if ($install_dir) {
			if (strpos($install_dir, '..') !== false) {
				throw new \Exception(sprintf("directory.install '%s' can’t use ..", $install_dir));
			}
			$working_install = $public_directory . '/' . $install_dir;
			if (!file_exists($working_install)) {
				@mkdir($working_install, $config->get('chmod'), true);
			}
			if (!file_exists($working_install)) {
				throw new \RuntimeException(sprintf('Impossible de créer le répertoire: %s', $install_dir));
			}
		}
		return true;
	}

	/**
	 * Gestion des droits d'acces
	 *
	 * @param string $self Nom du loader 'spip_loader.php' (phar) ou 'index.php' (en dehors)
	 * @param int $chmod
	 * @return int|false
	 */
	public function tester_repertoire($self, $chmod = 0755) {
		$ok = false;
		$uid = @fileowner('.');
		$uid2 = @fileowner($self);
		$gid = @filegroup('.');
		$gid2 = @filegroup($self);
		$perms = @fileperms($self);

		// Comparer l'appartenance d'un fichier cree par PHP
		// avec celle du script et du repertoire courant
		@rmdir('test');
		@unlink('test'); // effacer au cas ou
		@touch('test');
		if ($uid > 0 && $uid == $uid2 && @fileowner('test') == $uid) {
			$chmod = 0700;
		} else {
			if ($gid > 0 && $gid == $gid2 && @filegroup('test') == $gid) {
				$chmod = 0770;
			} else {
				$chmod = 0777;
			}
		}
		// Appliquer de plus les droits d'acces du script
		if ($perms > 0) {
			$perms = ($perms & 0777) | (($perms & 0444) >> 2);
			$chmod |= $perms;
		}
		@unlink('test');

		// Verifier que les valeurs sont correctes

		@mkdir('test', $chmod);
		@chmod('test', $chmod);
		$ok = (is_dir('test') && is_writable('test')) ? $chmod : false;
		@rmdir('test');

		return $ok;
	}
}
