<?php

namespace Spip\Loader\Check;

use Spip\Loader\Config\Config;
use Spip\Loader\Spip;
use Spip\Loader\Template\Page;

interface CheckInterface {
	/** @return bool */
	public function __invoke(Config $config, Spip $spip, Page $page);
}
