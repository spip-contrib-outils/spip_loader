<?php

namespace Spip\Loader\I18n;

class Translator {
	/** @var string */
	private $lang = 'fr';

	/** @var string */
	private $fallback_lang = 'fr';

	/** @var array<mixed> */
	private $langues = [];

	/** @var string */
	private $lang_directory;

	/**
	 * @param string $root_directory
	 * @param string $lang
	 * @param string $fallback_lang
	 */
	public function __construct($root_directory, $lang = '', $fallback_lang = '') {
		$this->lang_directory = $root_directory . '/lang';
		if ($lang) {
			$this->lang = $lang;
		}
		if ($fallback_lang) {
			$this->fallback_lang = $fallback_lang;
		}
	}

	/** @return string */
	public function getLang() {
		return $this->lang;
	}

	/**
	 * Translate a key, replace context paramaters
	 *
	 * Fallback on Languages::LANG_DEFAULT if not found
	 * Then returns '[%key%]' if not found
	 *
	 * @param string $key Translation key
	 * @param array<mixed> $context
	 * @param string $lang Destination language (default to object language)
	 * @return string
	 */
	public function translate($key, array $context = [], $lang = '') {
		return $this->contextualize($this->find($key, $lang), $context) ?: '[%' . $key . '%]';
	}

	/**
	 * Returns message associate to a translation key
	 *
	 * Fallback on self::$fallback_lang if not found
	 * Then returns '' if not found
	 *
	 * @param string $key Translation key
	 * @param string $lang Destination language (default to object language)
	 * @return string
	 */
	public function find($key, $lang = '') {
		$key = str_replace('tradloader:', '', $key);
		$lang = $lang ?: $this->lang;
		$this->load_language_file($lang);
		if (isset($this->langues[$lang][$key])) {
			return $this->langues[$lang][$key];
		}
		if (!$this->fallback_lang or $lang === $this->fallback_lang) {
			return '';
		}

		return $this->find($key, $this->fallback_lang);
	}

	/**
	 * Paramétrise un texte de traduction
	 *
	 * @param string $message
	 * @param array<mixed> $context
	 * @return string
	 */
	public function contextualize($message, array $context = []) {
		if (!$context or !strlen($message)) {
			return $message;
		}

		$keys = [];
		foreach (array_keys($context) as $key) {
			$keys[] = '@' . $key . '@';
		}

		return str_replace($keys, $context, $message);
	}

	/**
	 * Utiliser une autre langue
	 *
	 * @param string $lang
	 * @return void
	 */
	public function use_language($lang) {
		$this->lang = $lang;
		$this->load_language_file($lang);
	}

	/**
	 * Charger un fichier de langue.
	 *
	 * @param string $lang
	 * @return bool
	 */
	private function load_language_file($lang) {
		if (isset($this->langues[$lang])) {
			return true;
		}

		$fichier = 'tradloader_' . $lang . '.php';
		$GLOBALS['idx_lang'] = 'i18n_tradloader_' . $lang;
		$lang_file = $this->lang_directory . '/' . $fichier;
		if (!file_exists($lang_file)) {
			trigger_error(sprintf('Lang %s not found', $lang), \E_USER_WARNING);
			$this->langues[$lang] = [];
			return false;
		}
		$trads = include $lang_file;
		$this->langues[$lang] = is_array($trads) ? $trads : $GLOBALS[$GLOBALS['idx_lang']];
		unset($GLOBALS[$GLOBALS['idx_lang']], $GLOBALS['idx_lang']);
		return true;
	}
}
