<?php

namespace Spip\Loader\I18n;

class Languages {
	# langues disponibles
	const LANGUAGES = [
		'ar' => '&#1593;&#1585;&#1576;&#1610;',
		'ast' => 'asturianu',
		'br' => 'brezhoneg',
		'ca' => 'catal&#224;',
		'cs' => '&#269;e&#353;tina',
		'de' => 'Deutsch',
		'en' => 'English',
		'eo' => 'Esperanto',
		'es' => 'Espa&#241;ol',
		'eu' => 'euskara',
		'fa' => '&#1601;&#1575;&#1585;&#1587;&#1609;',
		'fr' => 'fran&#231;ais',
		'fr_tu' => 'fran&#231;ais copain',
		'gl' => 'galego',
		'hr' => 'hrvatski',
		'id' => 'Indonesia',
		'it' => 'italiano',
		'km' => 'Cambodian',
		'lb' => 'L&euml;tzebuergesch',
		'nap' => 'napulitano',
		'nl' => 'Nederlands',
		'oc_lnc' => '&ograve;c lengadocian',
		'oc_ni' => '&ograve;c ni&ccedil;ard',
		'pt_br' => 'Portugu&#234;s do Brasil',
		'ro' => 'rom&#226;n&#259;',
		'sk' => 'sloven&#269;ina',	// (Slovakia)
		'sv' => 'svenska',
		'tr' => 'T&#252;rk&#231;e',
		'wa' => 'walon',
		'zh_tw' => '&#21488;&#28771;&#20013;&#25991;', // chinois taiwan (ecr. traditionnelle)
	];

	const LANG_RTL = ['ar', 'he', 'fa'];

	const LANG_DEFAULT = 'fr';

	/**
	 * @param string $lang
	 * @return bool Cette langue est gérée ?
	 */
	public static function exists($lang) {
		return $lang && array_key_exists($lang, self::LANGUAGES);
	}

	/**
	 * Trouver une langue adaptée à la demande du navigateur,
	 * sinon la langue par défaut
	 *
	 * @param string $http_accept_language
	 * @return string
	 */
	public static function find_lang_from_http_accept_language($http_accept_language) {
		$accept_langs = explode(',', (string) $http_accept_language);
		if (is_array($accept_langs)) {
			foreach ($accept_langs as $s) {
				if (preg_match('#^([a-z]{2,3})(-[a-z]{2,3})?(;q=[0-9.]+)?$#i', trim($s), $r)) {
					$lang = strtolower($r[1]);
					if (self::exists($lang)) {
						return $lang;
					}
				}
			}
		}
		return self::LANG_DEFAULT;
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	public static function getLangDir($lang) {
		return in_array($lang, self::LANG_RTL) ? 'rtl' : 'ltr';
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	public static function getLangRight($lang) {
		return in_array($lang, self::LANG_RTL) ? 'left' : 'right';
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	public static function getLangLeft($lang) {
		return in_array($lang, self::LANG_RTL) ? 'right' : 'left';
	}
}
