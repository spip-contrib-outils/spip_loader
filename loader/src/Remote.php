<?php

namespace Spip\Loader;

use GuzzleHttp\Client;
use Spip\Loader\Config\Config;

class Remote {
	/** @var Config */
	protected $config;

	/** @var Client */
	protected $client;

	public function __construct(Config $config) {
		$this->config = $config;
		$options = [];
		if ($this->config->get('url.spip_loader.proxy')) {
			$options['proxy'] = $this->config->get('url.spip_loader.proxy');
		}
		$this->client = new Client($options);
	}

	/**
	 * @param string $url
	 * @return \Psr\Http\Message\StreamInterface
	 */
	public function get($url) {
		return $this->client->get($url)->getBody();
	}

	/**
	 * @param string $url
	 * @param string $destination_file
	 * @return void
	 */
	public function copyTo($url, $destination_file) {
		$this->client->get($url, ['sink' => $destination_file]);
	}

	/**
	 * Trouver le numéro de version du dernier spip_loader
	 * @return string|false
	 */
	public function recuperer_version_spip_loader() {
		static $version = null;
		if (is_null($version)) {
			$version = false;
			$spip_loader = trim($this->get($this->config->get('url.spip_loader.version')));
			if (preg_match('/^([0-9.]*)$/', $spip_loader, $m)) {
				$version = $m[1];
			}
		}
		return $version;
	}
}
