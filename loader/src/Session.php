<?php

namespace Spip\Loader;

class Session {
	// 10 minutes
	const DURATION = 600;

	public function __construct() {
		// Start the session if not started already
		if (session_status() == \PHP_SESSION_NONE) {
			session_start();
		}
	}

	/**
	 * @return void
	 */
	public function create() {
		// Set a session start time
		if (!isset($_SESSION['session_start_time'])) {
			$_SESSION['session_start_time'] = date('Y-m-d H:i:s');
		}

		// Create a session token
		if (empty($_SESSION['token'])) {
			// random_bytes(32) à la place de mt_rand() quand php mini >=7
			$_SESSION['token'] = bin2hex((string)mt_rand());
		}

		setcookie('spip_loader_token', $_SESSION['token'], time() + self::DURATION);
	}

	/** @return bool */
	public function checked() {
		if (empty($_COOKIE['spip_loader_token'])) {
			return false;
		}
		if (empty($_SESSION['token'])) {
			return false;
		}
		if (empty($_SESSION['session_start_time'])) {
			return false;
		}
		if ($_COOKIE['spip_loader_token'] !== $_SESSION['token']) {
			return false;
		}
		$start = new \Datetime($_SESSION['session_start_time']);
		if ($start->getTimestamp() + self::DURATION < time()) {
			return false;
		}

		return true;
	}

	/**
	 * @return void
	 */
	public function clean() {
		unset($_SESSION['token']);
		unset($_SESSION['session_start_time']);
		if (isset($_COOKIE['spip_loader_token'])) {
			setcookie('spip_loader_token', '', time() - 3600);
		}
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function has($key) {
		return isset($_SESSION[$key]);
	}

	/**
	 * @param string $key
	 * @return mixed|null
	 */
	public function get($key) {
		return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public function set($key, $value) {
		$_SESSION[$key] = $value;
	}
}
