<?php

namespace Spip\Loader;

use Spip\Loader\Iterator\SuperflusFilterIterator;

class Cleaner {
	/** @var int */
	private $chmod;

	/** @var string */
	private $dir_install;

	/** @var string */
	private $cleaner_keep_directory;

	/**
	 * @param string $dir_install
	 * @param int $chmod
	 * @param string $cleaner_keep_directory
	 */
	public function __construct($dir_install, $chmod, $cleaner_keep_directory) {
		$this->dir_install = $dir_install;
		$this->chmod = $chmod;
		$this->cleaner_keep_directory = $cleaner_keep_directory;
	}

	/**
	 * Déplace les fichiers qui sont en trop entre le contenu du zip et le répertoire destination.
	 *
	 * @param string $archive_filename Archive
	 * @param string $dir Répertoire où ils ont été copiés.
	 * @return bool
	 */
	public function nettoyer_superflus($archive_filename, $dir) {

		$dir_install = $this->dir_install;
		$chmod = $this->chmod;
		$diff = $this->comparer_contenus($archive_filename, $dir);
		if ($diff) {
			@mkdir($old = $dir_install . 'fichiers_obsoletes_' . date('Ymd_His'), $chmod);
			if (!is_dir($old)) {
				return false;
			}
			$old .= '/';
			foreach ($diff as $file => $isDir) {
				$root = $isDir ? $file : dirname($file);
				if (!is_dir($old . $root)) {
					mkdir($old . $root, $chmod, true);
				}
				if ($isDir) {
					Filesystem::move_all($dir_install . $root, $old . $root, $chmod);
					if (is_dir($dir_install . $root)) {
						rmdir($dir_install . $root);
					}
				} else {
					// Tout le répertoire a déjà pu être déplacé…
					// on vérifie la présence du fichier
					if (file_exists($dir_install . $file)) {
						rename($dir_install . $file, $old . $file);
					}
				}
			}
		}

		return true;
	}


	/**
	 * Retourne la liste des fichiers/répertoires en trop entre le zip et la destination,
	 * pour certains répertoires seulement.
	 *
	 * @param string $archive_filename le zip
	 * @param string $dir Répertoire ou le zip a été dézippé
	 * @return array<string,bool> Chemin superflu => isDir?
	 */
	public function comparer_contenus($archive_filename, $dir) {

		// On se considère dans SPIP et on vérifie seulement ces répertoires
		$repertoires_suivis = [
			'ecrire',
			'prive',
			'plugins-dist',
			'squelettes-dist',
			'vendor',
		];

		$contenus_source_suivis = $this->lister_contenus_zip_suivis($archive_filename, $repertoires_suivis);
		return $this->lister_contenus_superflus($contenus_source_suivis, $dir, $repertoires_suivis);
	}

	/**
	 * Retourne la liste des fichiers/répertoires suivis du zip
	 *
	 * @param string $archive_filename le zip
	 * @param array<mixed> $repertoires_suivis Répertoires que l'on vérifie (on ne s'occupe pas des autres)
	 * @return array<string,bool>|false chemin => isDir ?
	 */
	public function lister_contenus_zip_suivis($archive_filename, $repertoires_suivis) {
		// Liste des contenus sources (chemin => isdir?)
		$archive_filename = realpath($archive_filename);
		$phar_archive_root = 'phar://' . str_replace('\\', '/', $archive_filename) . '/';
		$contenus_source = [];

		foreach (new \RecursiveIteratorIterator(new \PharData($archive_filename)) as $file) {
			$fichier = str_replace($phar_archive_root, '', (string) $file);
			$root = explode('/', $fichier, 2);
			$root = reset($root);
			if (!in_array($root, $repertoires_suivis)) {
				continue;
			}
			$contenus_source[$fichier] = $file->isDir();
		}

		// certains zips n'indiquent pas les répertoires ; on les ajoute...
		// on ne conserve pas le / final du coup...
		foreach ($contenus_source as $fichier => $isDir) {
			if (!$isDir) {
				$_dir = dirname($fichier);
				if ($_dir === '.' or isset($contenus_source[$_dir])) {
					continue;
				}
				do {
					$contenus_source[$_dir] = true;
					$_dir = dirname($_dir);
				} while ($_dir && $_dir !== '.');
			}
		}

		return $contenus_source;
	}

	/**
	 * Liste les contenus en trop dans certains répertoires, en fonction d’une liste de fichiers
	 *
	 * Un répertoire superflu, mais contenant un fichier .spip_loader_keep est conservé,
	 * c'est à dire qu’il ne sera pas retourné dans cette liste de fichiers/répertoire obsolètes.
	 *
	 * @param array<string,bool> $contenus_source liste(chemin => isDir?)
	 * @param string $dir Chemin du répertoire à tester
	 * @param array<int,string>|null $repertoires_suivis Liste de répertoires à uniquement parcourrir si défini.
	 * @return array<string,bool> liste(chemin => isDir?) des fichiers/répertoire en trop.
	 */
	public function lister_contenus_superflus($contenus_source, $dir, $repertoires_suivis) {

		$iterator = new \AppendIterator();
		foreach ($repertoires_suivis as $repertoire_suivi) {
			if (is_dir($dir . $repertoire_suivi)) {
				$it = new \RecursiveDirectoryIterator($dir . $repertoire_suivi, \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::UNIX_PATHS);
				$it = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::SELF_FIRST);
				$iterator->append($it);
			}
		}

		$superflus = new SuperflusFilterIterator($iterator, $contenus_source, $dir, $this->cleaner_keep_directory);

		$liste = [];
		$ignoreLen = strlen($dir);
		foreach ($superflus as $file) {
			$liste[ (string) substr($file->getPathname(), $ignoreLen) ] = $file->isDir();
		}

		return $liste;
	}

	/**
	 * @param string $fichier
	 * @return bool
	 */
	public function nettoyer_racine($fichier) {
		@unlink($fichier);
		return true;
	}
}
