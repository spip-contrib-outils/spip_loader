<?php

use Spip\Loader\Application;
use Spip\Loader\Config\Config;
use Spip\Loader\Config\Internal;
use Spip\Loader\Config\Custom;
use Spip\Loader\Deprecations;
use Spip\Loader\Debug;
use Spip\Loader\Http\Request;
use Spip\Loader\Http\Response;
use Spip\Loader\Template\Error as TemplateError;
use Spip\Loader\Template\Utils;

require __DIR__ . '/../vendor/autoload.php';

try {
	$request = Request::createFromGlobals();

	$config = new Config(
		new Internal($request),
		(new Custom())->loadIfExists('spip_loader_config.php')
	);

	if ($config->get('debug')) {
		Debug::enable();
	}

	$deprecations = new Deprecations($config);
	$deprecations->handle();
	$app = new Application($config, $request);
	$app->run();
} catch (\Exception $e) {
	$template = TemplateError::fromException($e);
	if (isset($config)) {
		$template->setBaseUri($config->get('phar.filename'));
		$template->setFrontendUrl($config->get('url.frontend'));
		$template->setAppFilename($config->get('app.filename'));
	}
	$response = new Response($template->toHTML(), 400, Utils::http_no_cache());
	$response->send();
}

exit(0);
