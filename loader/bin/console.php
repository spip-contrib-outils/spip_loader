<?php

use Spip\Loader\Application;
use Spip\Loader\Config\Config;
use Spip\Loader\Config\Custom;
use Spip\Loader\Config\Internal;
use Spip\Loader\Http\Request;

require __DIR__ . '/../vendor/autoload.php';

$request = Request::createFromGlobals();
$config = new Config(
	new Internal($request, true),
	new Custom()
);

$app = new Application($config, $request);
$app->init();

$title = 'SPIP Loader ' . $app->getVersion();
$line = str_repeat('-', strlen($title));

echo <<<TEXT

$title
$line

TEXT;
