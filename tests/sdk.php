<?php

define('_FILE_CONNECT', '');

/**
 * Undocumented function
 *
 * @param string $f
 * @param boolean $include
 * @return string|bool
 */
function include_spip($f, $include = true) {
	return true;
}

/**
 * Undocumented function
 *
 * @param string $titre
 * @param string $onLoad
 * @param boolean $all_inline
 * @return string
 */
function install_debut_html($titre = 'AUTO', $onLoad = '', $all_inline = false) {
    return '';
}

/**
 * Undocumented function
 *
 * @return string
 */
function install_fin_html() {
	return '';
}

/**
 * Undocumented function
 *
 * @param string $texte
 * @param array $args
 * @param array $options
 * @return string
 */
function _T($texte, $args = [], $options = []) {
    return '';
}

/**
 * Undocumented function
 *
 * @param string $url
 * @param string $c
 * @param string|array|null $v
 * @param string $sep
 * @return string
 */
function parametre_url($url, $c, $v = null, $sep = '&amp;') {
    return '';
}

/**
 * Undocumented function
 *
 * @param string $script
 * @param string $args
 * @param boolean $no_entities
 * @param boolean $rel
 * @param string $action
 * @return string
 */
function generer_url_public($script = '', $args = '', $no_entities = false, $rel = true, $action = '') {
    return '';
}

function apc_delete_file($filename) {

}
