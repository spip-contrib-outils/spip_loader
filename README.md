# spip_loader

![Spip Loader](public_html/assets/spip_loader.svg)

**spip_loader** est un script qui permet d'installer [SPIP](https://spip.net/) ou de mettre à jour
votre site automatiquement sans avoir à transférer l'intégralité des fichiers par FTP.

Il nécessite PHP 5.6 ou plus.

## Télécharger

[https://get.spip.net/spip_loader.php](https://get.spip.net/spip_loader.php)

## Configuration

Un fichier `spip_loader_config.php` peut être placé au même niveau que le script
`spip_loader.php` pour définir une configuration, en retournant un tableau PHP.

### Exemple

L’exemple suivant autorise les auteurs n°1, 2 et 3 à utiliser le SPIP Loader
sur un site déjà existant, s’ils sont au moins administrateurs sur le site en question.

```php
<?php
return [
    'authorized.users' => [1, 2, 3],
];
```

### Configurations

D’autres configurations utilisateur sont possibles, mais `authorized.users` est probablement
la seule utilisée couramment. Voir [Config/Custom.php](loader/src/Config/Custom.php)

## Question

Si vous vous posez l'une de ces questions :

- Utiliser spip_loader pour installer automatiquement SPIP
- Utiliser spip_loader pour effectuer une mise à jour
- Personnaliser les auteurs autorisés à effectuer une mise à jour
- Que faire en cas d’échec ?

La réponse se trouve sur la documentation du loader sur le site de SPIP : [Utiliser spip_loader](https://www.spip.net/fr_article5705.html)

## Contribuer

Si vous souhaitez commiter sur ce projet, n'hésitez pas à proposer [une PR](https://git.spip.net/spip-contrib-outils/spip_loader/pulls)

Les traductions sont gérées [dans ce module](https://trad.spip.net/tradlang_module/tradloader)

## Compiler

Le fichier `spip_loader.php` est [un fichier Phar](https://www.php.net/phar) qui nécessite une compilation.

### Prérequis

- La version du script est automatiquement mise à jour à partir du dernier tag du dépôt. Il est important que ce tag ne comporte pas de `v` en prefix.
- [composer (v2)](https://getcomposer.org/) doit être présent sur la machine qui compile aussi.
- PHP >= 8.1 est requis pour la compilation

### Procédure

#### Cloner le dépot

```bash
git clone https://git.spip.net/spip-contrib-outils/spip_loader.git /path/to/spip_loader
```

#### Mettre à jour et installer

```bash
cd /path/to/spip_loader
git pull
composer install
composer install -d loader --no-dev
```

*Note:* Si `composer install` râle sur `composer.lock` racine, supprimer le fichier et recommencer.

#### Compiler le spip_loader

```bash
php -d phar.readonly=0 ./compile
```

#### Publier une version

```bash
git tag x.y.x
git push --tags
```

### Vérifications locales

L’archive `spip_loader.php` est compilé dans le répertoire `build/`

### Debug

Un mode debug peut être activé, qui affiche des informations (possiblement utiles pour comprendre
certains problèmes sur des hébergements spécifiques) sur chaque page du loader.

Il peut être activé

- via l’option `'debug' => true` dans `spip_loader_config.php`
- ou à la compilation (et plus précocément)

```bash
php -d phar.readonly=0 ./compile --debug
```

### Installation sur get.spip.net (public_html)

Pour copier le `spip_loader.php` en prod, l’option `--prod` peut être utilisée

```bash
php -d phar.readonly=0 ./compile --prod
```
