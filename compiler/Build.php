<?php

namespace Spip\Loader\Compiler;

use League\CLImate\CLImate;
use SebastianBergmann\Timer\Duration;
use SebastianBergmann\Timer\ResourceUsageFormatter;
use SebastianBergmann\Timer\Timer;

class Build {
	private array $stats = [];

	public function __construct(
		private string $sourceDirectory,
		private string $buildDirectory,
		private string $pharFilename,
		private Timer $timer = new Timer(),
		private bool $debug = false,
	) {
	}

	public function prepare(): void {
		if (!is_dir($this->buildDirectory)) {
			mkdir($this->buildDirectory);
		}

		foreach (
			[
				'spip_loader.php',
				'version',
				'sha1',
			] as $clean_file
		) {
			if (file_exists($this->buildDirectory . '/' . $clean_file)) {
				unlink($this->buildDirectory . '/' . $clean_file);
			}
		};
	}

	public function run() {
		$this->start();
		$git = new Git(dirname(__DIR__));
		$phar = new PharArchive(
			sourceDirectory: $this->sourceDirectory,
			buildDirectory: $this->buildDirectory,
			debug: $this->debug,
		);
		$this->stats['Git Version'] = $git->getFullVersion();
		$this->stats['Git Date'] = $git->getVersionDate()->format('Y-m-d H:i:s');
		$n = $phar->build(
			$git->getVersion(),
			$git->getFullVersion(),
			$git->getVersionDate()
		);
		$this->stats['Nb files'] = $n;
		$phar->renameTo($this->pharFilename);
		file_put_contents($this->buildDirectory . '/version', $git->getVersion());
		file_put_contents($this->buildDirectory . '/sha1', \sha1_file($this->buildDirectory . '/' . $this->pharFilename));
		$this->stop();
	}

	public function showResults() {
		$climate = new CLImate();
		$climate->out('');
		$climate->underline()->out('Results');
		$climate->out('');
		foreach ($this->stats as $item => $value) {
			$climate->infoInline("$item: ");
			$climate->out($value);
		}
		$climate->out('');
	}

	public function showCopyTo(string $prodDirectory) {
		$climate = new CLImate();
		if ($this->copyTo($prodDirectory)) {
			$climate->info('> Fichiers copiés en prod');
		} else {
			$climate->error('! Échec de la recopie en prod');
		}
		$climate->out('');
	}

	public function copyTo(string $prodDirectory) {
		if (!is_dir($prodDirectory)) {
			throw new \Exception(sprintf('Directory %s needs to exists', $prodDirectory));
		}
		$files = [$this->pharFilename, 'version', 'sha1'];
		foreach ($files as $file) {
			if (!file_exists($this->buildDirectory . '/' . $file)) {
				throw new \Exception(sprintf('File %s is not compiled ?', $this->buildDirectory . '/' . $file));
			}
		}
		foreach ($files as $file) {
			if (file_exists($prodDirectory . '/' . $file)) {
				unlink($prodDirectory . '/' . $file);
			}
			copy($this->buildDirectory . '/' . $file, $prodDirectory . '/' . $file);
		}
		foreach ($files as $file) {
			if (!file_exists($prodDirectory . '/' . $file)) {
				return false;
			}
		}
		return true;
	}

	public function getStats(): array {
		return $this->stats;
	}

	private function start() {
		$this->stats = [
			'Version' => 'No version file',
			'Git Version' => '',
			'Git Date' => '',
			'Time' => '',
			'Memory' => '',
			'Filesize' => 'No archive file',
			'sha1' => '',
			'Nb files' => '',
		];
		$this->timer->start();
	}

	private function stop() {
		$this->saveStats($this->timer->stop());
	}

	private function saveStats(Duration $duration) {
		[$time, $memory] = $this->parseResourceUsage($duration);
		$this->stats['Time'] = $time;
		$this->stats['Memory'] = $memory;
		$pharPath = $this->buildDirectory . '/' . $this->pharFilename;
		if (file_exists($pharPath)) {
			$this->stats['Filesize'] = $this->MakeReadable(filesize($pharPath));
		}
		if (file_exists($this->buildDirectory . '/version')) {
			$this->stats['Version'] = file_get_contents($this->buildDirectory . '/version');
		}
		if (file_exists($this->buildDirectory . '/sha1')) {
			$this->stats['sha1'] = \sha1_file($pharPath);
		}
	}

	private function parseResourceUsage(Duration $duration): array {
		$usage = (new ResourceUsageFormatter())->resourceUsage($duration);
		[$time, $memory] = explode(', ', $usage, 2);
		return [
			explode(': ', $time)[1],
			explode(': ', $memory)[1],
		];
	}

	private function MakeReadable($bytes) {
		$i = floor(log($bytes, 1024));
		return round($bytes / pow(1024, $i), [0,0,2,2,3][$i]) . ['B','kiB','MiB','GiB','TiB'][$i];
	}
}
