<?php

namespace Spip\Loader\Compiler;

use DateTimeImmutable;
use DateTimeZone;

class Git {
	public function __construct(private string $directory = __DIR__) {
	}

	public function setDirectory(string $directory) {
		$this->directory = $directory;
	}

	public function run(string $command, $env = null): string {
		$git = ($env ? $env . ' ' : '') . 'git -C ' . $this->directory;
		return  trim(exec($git . ' ' . $command));
	}

	public function getVersion(): string {
		return $this->run('describe --tags --abbrev=0');
	}

	public function getFullVersion(): string {
		return $this->run('describe --tags --abbrev=1');
	}

	public function getVersionDate(): DateTimeImmutable {
		$date = $this->run('show --quiet --date=local --format="%cd" HEAD', 'TZ=UTC0');
		$date = new DateTimeImmutable($date ?: 'now', new DateTimeZone('UTC'));
		return $date;
	}
}
