# Changelog

## 6.1.8 - 2025-01-20

### Fixed

- Gérer la présence et l'inclusion de l'autoloader de SPIP pour les mises à jour

## 6.1.6 - 2024-02-20

### Fixed

- #53 Éviter une erreur sur le nettoyage avec SPIP < 4.2 depuis la version 6.1.4

## 6.1.5 - 2024-02-10

### Fixed

- Rediriger dans l’espace privé d’une manière qui actualise la version dans le pied de page

## 6.1.4 - 2024-02-08

### Fixed

- Accélérer le nettoyage des fichiers obsolètes

## 6.1.3 - 2023-09-21

### Fixed

- #49 Correction de la compilation du phar sous Windows
- #46 Éviter une page blanche sur un self update du loader.

## 6.1.2 - 2023-06-09

### Fixed

- #39 (bis) Ne (vraiment) pas tenter la correction décompression de l’archive SPIP en PHP < 7.4.15
- #44 Bien conserver tout le répertoire & sous répertoires contenant `.spip_loader_keep`

## 6.1.1 - 2023-05-03

### Fixed

- #39 Correction décompression de l’archive SPIP en PHP < 7.4.15
- #39 Chargement du Loader en PHP 5.6
- #37 Syntaxe pour PHP < 7.3

## 6.1.0 - 2023-04-12

### Added

- La config `debug` à `true` affichera systématiquement des informations en pied de page
- La compilation avec `--debug` activera le mode debug (plus tôt dès le `stub`)

### Changed

- Spinner à la place de la barre de progression (trop peu d’étapes)
- Dézippage via PharData
- Téléchargements via Guzzle
- Le Phar reste dans l’url `spip_loader.php` sans rediriger sur `spip_loader.php/index.php` (pour Nginx)
- Les assets sont chargées avec `?file=` dans le Phar (pour Nginx)

### Fixed

- Vérification de l’auteur connecté sur les SPIP < 4.0
- Fonctionnement avec Nginx dans sa conf par défaut (il n’aime pas les `fichier.php/autre_fichier.ext`)
- Erreur de lecture des Phar dans certains caches (opcache) chez certains hébergeurs : on le désactive

### Removed

- PclZip !


## 6.0.3 - 2023-03-29

### Fixed

- Redirection infinie chez certains hébergeurs
- Mauvaise définition de PHP_SELF chez certains hébergeurs

## 6.0.2 - 2023-03-09

### Added

- Route '?etape=debug' (nécessite `'debug' => true` dans `spip_loader_config.php` pour y accéder)
- Ajout de la version aussi en entête du stub du Phar.

### Changed

- Meilleure gestion du selfupdate

### Fixed

- #29 Corriger PHP_SELF dans le Phar chez certains hébergements

## 6.0.1 - 2023-03-09

### Fixed

- Fatal error lors d’un deprecated de config.s

## 6.0.0 — 2023-03-09

### Fixed

- #26 PCLZip tolérant à memory_limit = -1 (on lui affecte 128m dans ce cas)
- #19 #22 Redirections du loader à la bonne URL

### Changed

- Vérifier la version du SPIP installé peut se faire sans démarrer le SPIP.
- Barre de progression plus homogène (pas uniquement sur le dézippage)
- Les vérifs (authentification à un SPIP existant, droits d’écriture, extensions requises) ne sont faite qu’une fois sur la page d’accueil (jusqu’à ce que tout soit bon)
- Une session + jeton est accordé pour 10mn si les vérifs sont OK.
- #9 Le HTML de la page utilise un style plus proche de Minipage de SPIP 4.2.
- L’archive phar peut fournir des ressources css ou images (avec un expire, et tolérant à HTTP/2)
- La configuration via `spip_loader_config.php` : le fichier doit retourner un `array`
- Refonte assez conséquente du code (mais utilise encore des vieux trucs, notamment pclzip toujours là !)
- Découpage du gros fichier d’origine en architecture PSR-4 dans src/
- PHP 5.6 minimum requis

## Deprecated

- Constante `_SPIP_LOADER_UPDATE_AUTEURS`. Utiliser `update.authors` en config. Tel que `'update.authors' => [1, 2],`

## 5.x

- 5.3.0 : Se prémunir d'une boucle de redirection avec php-fpm
- 5.2.1  : Réparer le lien de login ainsi que le chargement des CSS quand on affiche le loader sans être identifié
- 5.2.0  : Distribution du script en version compréssée (phar) @see `compile` file
- 5.1.0  : Automatisation de la génération du fichier spip_loader_list.json
- 5.0.1  : Montrer une erreur si on ne peut pas écrire la mise à jour de Spip Loader, invalider les caches fichiers du loader sur  mise à jour.
- 5.0.0  : Version 2 de l’api JSON qui inclut la branche par défaut et les requirements php.

## 4.x

- 4.3.3  : SPIP 4.0 par défaut
- 4.3.2  : Compat PHP de SPIP 4.0
- 4.3.1  : Toutes les fonctions du loader son préfixées par SL.
           On fait un tour au déballage fini, à 100% pour avoir tout le temps libre pour déplacer les fichiers et vérifier les superflus.
- 4.3.0  : Correction pour retrouver l'analyse et déplacement des fichiers obsolètes
           SL Nécessite PHP 5.2 minimum (faut pas pousser... désolé les vieux SPIP 3.1...)
- 4.2.0  : Mise en cache pour 10mn de spip_loader_list.json
- 4.1.1  : Tolérance php < 5.4 du spip_loader !
- 4.1.0  : Si notre SPIP installé actuel n’est plus une version maintenue, on demande explicitement la branche
- 4.0.0  : Utilisation d’un fichier spip_loader_list.json
           On ne rend disponible que les dernières versions maintenues.
